! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: slides help.markup ;
IN: german-fig-talk

CONSTANT: german-fig-slides
{

{ $slide "arrayFactor - a GA144 simulator"

"A GA144 simulator written in Forth."
"By Leon Konings - Konings Software"
"President of the Dutch HCC Forth Interest Group"
"Email: leon@koningssoftware.com"
}

{ $slide "The GA144 chip"

"144 small computers on one chip"
"Very fast and very low energy consumption\n"
"Invented by: Mr. Charles Moore"
"Produced by: GreenArrays, Inc."
}

{ $slide "Great respect for:"

"Mr. Charles Moore as inventor of Forth and the GA144."
"The people at GreenArrays, Inc."
"Who are some of the best (Forth) programmers in the world."
}

{ $slide "About me, Leon Konings"

"Programming Forth since about 1983."
"Only for a few years use Forth in combination with hardware."
"Love Forth and Factor - Like many other languages"
"Living in Leiden, The Netherlands"
}

{ $slide "Why Forth?"

"Interactive, incremental programming"
"Low and high level programming with one language."
"Extremely flexible"
"Great for exploration"
}

{ $slide "Why Factor?"

"Forth and Lisp are both extremely smart languages."
"Factor is combination of these two, so it must be great too."
"Very active, smart, friendly and young community."
"It has many useful libraries."
"It is object oriented."
"Created by: Slava Pestov"
}

{ $slide "Some history"

"Started out with SeaForth 40C18."
"Moved on to GA144 and arrayForth."
"Simular chips, very different IDE"
}

{ $slide "Why build a GA144 simulator in Factor?"

"Was looking for a Factor project."
"Needed more tools to understand GA144 better."
}

{ $slide "Goals"

"More tools for:"
"Visualising data flow"
"Linking nodes"
"Debugging"
}

{ $slide "The GreenArrays IDE"

"Works with the colorForth editor."
"At the moment mainly supports block files."
"Has a very fast simulator."
"IDE is connected to two GA144's."
}

{ $slide "arrayFactor Simulator"

"Runs in Factor environment, so it uses text files."
"Does not have a colorForth editor yet."
"Simulator is slow, but offers much information."
"No connection to any GA144 yet."
"Would like to become a complete IDE."
}

{ $slide "The GA144 chip"

"Contains 144 little computers."
"These little computers are also called nodes."
"The nodes are layed out in a matrix."
"This matrix has 8 rows by 13 colums."
"Nodes at the edge often can do I/O."
}

{ $slide "Is arrayFactor too high level?"

"True, for nodes that do I/O."
"Maybe, for nodes that don't."
}

{ $slide "Advantage of arrayFactor"

"Look at problems from many angles."
"By creating output screens, logs and reports."
}

{ $slide "Example: The circular stack"

  { $code "USE: arrayfactor.projects.circular-stack" }
  "Windows with info about a node can be selected, from the popup-menu."
  "Click a node number with the right button to get a menu."
  "Click on 'Steps' button in Navigation Window to see the circular stack at work."
}

{ $slide "Some help"

  { $code "&&about" }
  { $code "&&about-console" }
} 

{ $slide "arrayFactor going Open Source"

"This project is to big for a few developers."
"Within a few days it will be available at Bitbucket."
"Follow the next link: "
{ $url "http://arrayFactor.org" }
}

{ $slide "Thanks for your attention"

"Would like it very much, if some of you got involved in this new open source project."

"Best regards,"

"Leon"
}
}

: german-fig-talk ( -- ) german-fig-slides slides-window ;

MAIN: german-fig-talk