#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrays formatting grouping io kernel math
math.ranges namespaces sequences vectors ;
IN: arrayfactor.chip.path

SYMBOL: Path

TUPLE: path
    { path0 vector }
    { path1 vector }
    { path2 vector } ;

: append-horizontal-path ( seq a b -- seq' )
    [a,b] append ;

: append-vertical-path ( seq a b -- seq' )
    2dup <= [ 100 ] [ -100 ] if
    <range> >array append ;

: 0pa ( -- seq )
    { } clone
    708 700 append-horizontal-path
    600   0 append-vertical-path
      1  17 append-horizontal-path
    117 717 append-vertical-path
    716 709 append-horizontal-path ;

: 1pa ( -- seq )
    { 708 608 } ;

: 2pa ( -- seq )
    { } clone
    708 717 append-horizontal-path
    617  17 append-vertical-path
     16   1 append-horizontal-path
      0 100 append-vertical-path
    101 116 append-horizontal-path
    216 200 append-horizontal-path
    300 316 append-horizontal-path
    416 400 append-horizontal-path
    500 516 append-horizontal-path
    616 600 append-horizontal-path
    700 707 append-horizontal-path ;

: <path> ( -- path )
    0pa 1pa 2pa
    path boa dup Path set ;

: print-path ( path -- )
    18 group [ [ "%03d" printf bl ] each nl ] each ;

! USE: arrayfactor.chip.path
