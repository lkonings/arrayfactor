#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays bit-arrays combinators
io kernel literals locals logging math math.bitwise models
namespaces prettyprint quotations sequences shuffle strings
vectors words words.symbol parser
arrayfactor.chip.chip-type
arrayfactor.chip.debug-info
arrayfactor.core.files
arrayfactor.chip.models
arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.core.macros
arrayfactor.core.utils ;
IN: arrayfactor.chip

TUPLE: chip
    { name string }
    { type chip-type }
    { nodes array }
    { ports array }
    { macros macros }
    { path string }
    { named-smart-integers named-smart-integers }
    { step integer }
    { halt boolean }
    { breaking boolean }
    { executing-now }
    { this-node model }
    { debug-info debug-info }
    { contains-models vector }
    { hook quotation }
    ui ;

: port-array-length ( rows cols -- length )
    1 + over 2 * 1 + * swap - 1 - ;

: <<chip>> ( name type -- chip )
                                 #! name
    dup                          #! <ga144> or other chip-type
    [ rows>> ] [ cols>> ] bi
    [ * f <array> ] 2keep        #! nodes
    port-array-length f <array>  #! ports
    <macros>                     #! macros
    ""                           #! path
    <named-smart-integers>       #! named-smart-integers
   -1                            #! step
    f                            #! halt
    f                            #! breaking
    f                            #! executing-now
    f <model>                    #! this-node
    <debug-info>                 #! debug-info
    V{ } clone                   #! contains-models 
    [ ]                          #! hook
    f                            #! ui
    chip boa
    dup Chips push
    Chips length 1 - Chip# set-global ;

: <chip> ( type -- chip )
    "" swap <<chip>> ;

: get-type ( chip -- type )
    type>> ; inline

: get-nodes ( chip -- nodes )
    nodes>> ; inline

: init-nodes ( chip -- )
    [ nodes>> length f <array> ] [ nodes<< ] bi ;

: get-node ( nd chip -- node/f )
    nodes>> nth ; inline

: chip-contains-models ( model -- )
    this-chip contains-models>> push ; inline

: nd>rc ( nd -- row col )
    this-chip type>> get-cols /mod ;

: nd>node# ( nd -- node# )
    nd>rc swap 100 * + ;

: node#>nd ( node# -- nd )
    100 /mod swap this-chip type>> get-cols * + ;

: node#>node ( node# -- node/f )
    node#>nd this-chip get-node ;

: init-ports ( chip -- )
    [ ports>> length f <array> ] [ ports<< ] bi ; inline

: init-arrays ( chip -- )
    [ init-nodes ] [ init-ports ] bi ;

: prt>port ( prt chip -- port )
    ports>> nth ; inline

: get-named-smart-integers ( chip -- named-smart-integers )
    named-smart-integers>> ; inline

: add-chip-macro ( code name -- )
    this-chip macros>> add-macro ;

: read-chip-macro ( relative-path name -- )
    [ read-file ] dip add-chip-macro ;

: get-step ( chip -- step )
    step>> ; inline

: this-node-model-> ( chip -- model )
    this-node>> ; inline

: this-node<- ( ? chip -- )
    this-node-model-> set-model ; inline

: ->this-node ( chip ? -- chip )
    over this-node<- ; inline

: this-node-> ( chip -- ? )
    this-node-model-> value>> ; inline
    
: dbg-get ( key -- value ? )
    this-chip debug-info>> get-debug-info ;

: dbg-set ( value key -- )
    this-chip debug-info>> set-debug-info ;

: dbg-inc ( key -- )
    this-chip debug-info>> inc-debug-info ;

: dbg-dec ( key -- )
    this-chip debug-info>> dec-debug-info ;

: ?>notify-connections-flags ( ? -- )
    this-chip contains-models>>
    swap [ >>notify-connections drop ] curry each ;

ALIAS: >ntf ?>notify-connections-flags

SYNTAX: CHIP. this-chip . ;

#! STORE

: node! ( nd chip -- )
    [ get-node ] [ this-node<- ] bi ;

: >current-node ( node# -- )
    node#>nd this-chip node! ;
