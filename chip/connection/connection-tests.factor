! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: math tools.test namespaces sequences kernel quotations vectors
arrayfactor.chip
arrayfactor.chip.connection
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.smart-integer
arrayfactor.chips ;
IN: arrayfactor.chip.connection.tests


#! "arrayfactor.chip.connection" test
