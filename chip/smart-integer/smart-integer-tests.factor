! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: arrayfactor.chip arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.smart-integer arrayfactor.chips combinators
kernel namespaces quotations sequences tools.test vectors ;
IN: arrayfactor.chip.smart-integer.tests

<ga144> <chip> drop

[ t ]
[ 5 <smart-integer>
  "name1" over >name
  "name1" this-chip get-named-smart-integers add>named-smart-integers
  "name1" this-chip get-named-smart-integers name>smart-integer? nip
]
unit-test

[ "$f" "#15" "%1111" ]
[ 15 <smart-integer>
  {  [ base>string ]
     [ 10 swap base< ]
     [ base>string ]
     [ 2 swap base< ]
     [ base>string ]
  } cleave
]
unit-test

[ 5 6 ]
[ 5 <smart-integer> ?n>
  6 ?n>
]
unit-test

remove-chip

#! "arrayfactor.chip.smart-integer" test
