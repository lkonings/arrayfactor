! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations assocs hashtables kernel locals
math math.bitwise math.parser namespaces sequences strings
arrayfactor.core.utils ;
IN: arrayfactor.chip.smart-integer

#!
#! SMART-INTEGER
#!

TUPLE: smart-integer
    { n integer }
    { name/f }
    { sequence# integer }
    { a-port boolean }
    { a-return-addr boolean }
    { base integer }
    { color-name/f } ;

: 18bits ( n -- n' )
    HEX: 3ffff bitand ; inline

: 18bits-sign-extend ( n -- n' )
    18bits 18 >signed ; inline

: <<smart-integer>> ( n name -- smart-integer )
!   dup [ >string ] when
    1            #! sequence#
    f            #! a-port
    f            #! a-return-addr
    16           #! base
    f            #! color-name/f
    smart-integer boa ;

: <<named-smart-integer>> ( n name -- smart-integer )
    <<smart-integer>> ; inline

: <named-smart-integer> ( n name -- smart-integer )
    [ 18bits-sign-extend ] dip <<smart-integer>> ;

: <smart-integer> ( n -- smart-integer )
    f <named-smart-integer> ;

: n> ( smart-integer -- n )
    n>> ; inline

: ?n> ( smart-integer? -- n )
    dup smart-integer? [ n> ] when ;

: 2n> ( smart-integer1 smart-integer2 -- n1 n2 )
    [ n> ] bi@ ; inline

: 2n-signed> ( smart-integer1 smart-integer2 -- n1 n2 )
    [ n> 18bits-sign-extend ] bi@ ; inline

: >n ( n smart-integer -- )
    [ 18bits-sign-extend ] dip n<< ;

: 18bits>n ( n smart-integer -- )
    [ 18bits ] dip n<< ;

: dec-smart-integer ( smart-integer -- )
    [ n>> 1 - ] [ >n ] bi ;

: inc-smart-integer ( smart-integer -- )
    [ n>> 1 + ] [ >n ] bi ;

: named? ( smart-integer -- ? )
    name/f>> f = not ;

: name> ( smart-integer -- name )
    name/f>> ; inline

: >name ( name/f smart-integer -- )
    name/f<< ; inline

: inc-sequence# ( smart-integer -- )
    [ 1 + ] change-sequence# drop ;

: sequence#> ( smart-integer -- sequence# )
    sequence#>> ;

: a-return-addr? ( smart-integer -- ? )
    a-return-addr>> ; inline

: >a-return-addr ( ? smart-integer -- )
    a-return-addr<< ; inline

: is-return-addr ( smart-integer -- )
    t >>a-return-addr drop ; inline

: base>char ( n -- c )
    "==%=======#=====$" nth 1string ;

: base>string  ( smart-integer -- string )
    [ n> ] [ base>> ] bi
    [ >base ] keep
    base>char prepend ; inline

: base< ( n smart-integer -- )
    base<< ;

! : nm>t ( name node -- ) t> >name ;

! name>t
! name>s
! name>r
! name>a
! name>b
! name>port

: (smart)integer>number ( x -- n )
    dup smart-integer? [ n> ] when ;

: smart-integer>string ( smart-integer -- string )
    dup named?
    [ [ n> >hex " " append ] [ name> ] bi append ]
    [ n> >hex ]
    if ;

#!
#! NAMED-SMART-INTEGERS
#!

TUPLE: named-smart-integers { lib hashtable } ;

: <named-smart-integers> ( -- named-smart-integers )
    H{ } clone   #! lib
    named-smart-integers boa ;

: add>named-smart-integers ( smart-integer name named-smart-integers -- )
    lib>> set-at ;

: name>smart-integer? ( name named-smart-integers -- smart-integer/f ? )
    lib>> at* ;

: exclusively-named? ( smart-integer1 smart-integer2 -- ? )
    [ named? ] bi@ xor ; inline

#! If two named smart-integers come together, they both lose their name.
#! If only one is named, they merge using this name.
: smart-integer-naming ( integer3 smart-integer1 smart-integer2 -- smart-integer3 )
    2dup exclusively-named?
    [ [ name> ] bi@ append ] [ 2drop f ] if <<named-smart-integer>> ; inline

: >named?-smart-integer ( n smart-integer -- smart-integer' )
    name> <<named-smart-integer>> ; inline

#!
#! INTEGER-NAME
#!

TUPLE: integer-name
    { register string }
    { the-name string } ;

: <integer-name> ( register name -- integer-name )
    integer-name boa ;

! USE: arrayfactor.chip.smart-integer
