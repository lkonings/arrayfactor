#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrayfactor.chip
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.memory
arrayfactor.chip.node.f18a.memory.debug
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.node.f18a.status
arrayfactor.chip.port
arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.core.utils
classes.tuple
kernel locals logging math math.parser memory models
namespaces sequences threads vectors ;
IN: arrayfactor.chip.nodes

#!
#! TIMED-EXECUTION AND STEPPING
#!

SYMBOLS: Executing Executing-Nd ;

: start-timed-executes ( chip -- )
    f >>executing-now
    nodes>> sift
    [ Executing-Nd set
      dup Executing set
      start-timed-execute
    ] each-index ;

: execute-timed-opcode ( opcode node -- )
    over break?
    [ 2drop ]
    [ over empty?
      [ nip block-or-accept?! ]
      [ swap (execute-timed-opcode)
      ] if
    ] if ;

: timed-execute ( node -- )
    dup stacks>> stopped>>
    [ drop ]
    [ dup [ status>> inc-(not-)blocking-count ] [ blocking? ] bi
      [ drop ]
      [ [ opcode-> first ] [ execute-timed-opcode ] [ next-opcode ] tri
      ] if
    ] if
    yield ;

: notify-all-models ( -- ) ;
!   this-chip contains-models>>
!   [ tuple-slots [ model? ] filter [ notify-connections ] each ] each

: sometimes? ( step -- ? )
    10 mod 9 = ; inline

: gc?! ( step -- ) drop ;
!   500 mod 499 = [ gc ] when

LOG: index-input DEBUG
LOG: step-input DEBUG

: (timed-executes) ( index node chip -- )
!   step>> [ gc?! ] [ sometimes? [ notify-all-models ] when ] bi
    drop
    over Executing set       
    Executing-Nd set
    timed-execute ;

: timed-executes ( chip -- )
    dup t >>executing-now
    dup breaking>>
    [ 2drop ]
    [ dup hook>> call( -- )
!     dup step>> dup 7300 > [ step-input ] [ drop ] if
      dup nodes>> sift
      [ pick halt>>
        [ 2drop ] [ pick (timed-executes) ] if
      ] each-index
      f >>halt drop
      f >>executing-now drop
    ] if ;

: error-node-data ( -- data )
    Executing-Nd get
    [ number>string " " append ] [ this-chip get-node name>> ] bi append ;

: set-step ( n -- )
    this-chip step<< ;

: inc-step ( -- )
    this-chip [ 1 + ] change-step drop ;

: start-stepping ( -- )
    0 set-step
    this-chip start-timed-executes ;

: (steps) ( n -- )
    this-chip
    f >>halt executing-now>>
    [ drop ] [ [ inc-step this-chip timed-executes ] times ]
    if ; inline

: steps ( n -- )
    dup 50,000 >
    [ f ?>notify-connections-flags
      (steps) notify-all-models ]
    [ (steps) ]
    if ;

:: step-until-active ( node# limit -- )
    node# node#>nd :> nd
    this-chip :> chip
    this-chip step>> limit + :> steps-limit
    [ chip inc-step timed-executes
      nd chip nodes>> nth
      [ nd>> nd = ] [ status>> blocking-> not ] bi and
      this-chip [ step>> steps-limit > ] [ breaking>> ] bi
      or or not
    ] loop ;

:: step-until-p ( node# p limit -- )
    node# node#>nd :> nd
    this-chip :> chip
    this-chip step>> limit + :> steps-limit
    [ chip inc-step timed-executes
      nd chip nodes>> nth
      [ nd>> nd = ] [ p> p = ] bi and
      this-chip [ step>> steps-limit > ] [ breaking>> ] bi
      or or not
    ] loop ;

:: step-until-t ( node# t limit -- )
    node# node#>nd :> nd
    this-chip :> chip
    this-chip step>> limit + :> steps-limit
    [ chip inc-step timed-executes
      nd chip nodes>> nth
      [ nd>> nd = ] [ stacks>> t> n> t = ] bi and
      this-chip [ step>> steps-limit > ] [ breaking>> ] bi
      or or not
    ] loop ;

:: step-until-stopped ( node# limit -- )
    node# node#>nd :> nd
    this-chip :> chip
    this-chip step>> limit + :> steps-limit
    [ chip inc-step timed-executes
      nd chip nodes>> nth
      [ nd>> nd = ] [ stacks>> stopped>> ] bi and
      this-chip [ step>> steps-limit > ] [ breaking>> ] bi
      or or not
    ] loop ;

:: step-until-addr-contains ( node# addr n limit -- )
    node# node#>nd :> nd
    this-chip :> chip
    this-chip step>> limit + :> steps-limit
    [ chip inc-step timed-executes
      nd chip nodes>> nth
      [ nd>> nd = ] [ addr swap memory>data n = ] bi and
      this-chip [ step>> steps-limit > ] [ breaking>> ] bi
      or or not
    ] loop ;

:: step-until-port-active ( node1# node2# limit -- )
    !TODO Not useful as is...
    node1# node2# [ node#>nd ] bi@
    nds>prt this-chip prt>port dup
    [ :> the-port
      this-chip :> chip
      this-chip step>> limit + :> steps-limit
      [ chip inc-step timed-executes
        the-port active-node>> -1 = not
        this-chip [ step>> steps-limit > ] [ breaking>> ] bi
        or or not
      ] loop
    ] [ drop ] if ;

! USE: arrayfactor.chip.nodes
