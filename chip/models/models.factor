! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors kernel models ;
FROM: models => change-model ;
IN: arrayfactor.chip.models

TUPLE: notify-connections-flag { notify-connections boolean initial: t } ; 

: set-model** ( value container model -- )
    swap notify-connections>>
    [ set-model ] [ value<< ] if ;

: change-model** ( container model quot -- )
    pick notify-connections>>
    [ change-model ] [ ((change-model)) value<< ] if drop ; inline

: notify-connections** ( container model -- )
    swap notify-connections>>
    [ notify-connections ] [ drop ] if ;
