#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays assocs combinators
fry kernel literals locals math models
namespaces sequences strings vectors words words.symbol
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.models
arrayfactor.chip.node
arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.core.utils ;
FROM: arrayfactor.core.utils => ?first ;
IN: arrayfactor.chip.port

SYMBOL: Port

#! The active node does the requests.

TUPLE: port
    { nd1 integer }
    { nd2 integer }
    { p-value model }
    { values-history model }
    { values integer }
    { active-node integer }
    { read-request boolean }
    { write-request boolean }
    { request-accepted boolean }
    { read-count integer }
    { notify-connections boolean } ;

: <port> ( nd1 nd2 -- port )
    2dup > [ swap ] when
    f <model>            #! p-value
    V{ } clone <model>   #! values-history
    30                   #! values
   -1                    #! active-node
    f                    #! read-request
    f                    #! write-request
    f                    #! request-accepted
    0                    #! read-count
    t                    #! notify-connections
    port boa dup Port set
    dup chip-contains-models ;

: reset-port ( port -- )
    t                   >>notify-connections
    f <model>           >>p-value
   -1                   >>active-node
    f                   >>read-request
    f                   >>write-request
    f                   >>request-accepted
    drop ;

: p-value-model-> ( port -- model )
    p-value>> ; inline

: p-value-> ( port -- value  )
    p-value-model-> value>> ; inline

: p-value<- ( value port -- )
    dup p-value-model-> set-model** ; inline

: ->p-value ( port value -- port )
    over p-value<- ; inline

: values-history-model-> ( port -- model )
    values-history>> ; inline

: values-history-> ( port -- vector  )
    values-history-model-> value>> ; inline

: values-history<- ( vector port -- )
    dup values-history-model-> set-model** ; inline

: ->values-history ( port vector -- port )
    over values-history<- ; inline

: read-count-inc ( port -- )
    [ 1 + ] change-read-count drop ; inline

: set-active-node ( n port -- )
    active-node<< ; inline

: get-active-node ( port -- n )
    active-node>> ; inline

: get-read-count ( port -- n )
    read-count>> ; inline

: get-request-accepted ( port -- ? )
    request-accepted>> ; inline

: active-node? ( port -- ? )
    active-node>> 0 >= ; inline

: =active-node? ( nd port -- ? )
    active-node>> = ; inline

ERROR: writing-to-not-connected-port-error ;
ERROR: deadlock-writing-error ;

: port>nds-array ( port -- array )
    [ nd1>> ] [ nd2>> ] bi
    [ nd>node# ] bi@
    2dup > [ swap ] when 2array ;

:: set-port-value-from-node ( v port nd -- )
    port [ active-node>>
    [ -1 = not ] [ nd = not ] bi and ] [ write-request>> ] bi and
    [ port port>nds-array "Deadlock-Writing-Nodes"
      dbg-set deadlock-writing-error ] when
    v port p-value<- ;

#!
#! PORT HISTORY
#!

TUPLE: port-integer
    { smart-integer smart-integer }
    { active-node integer }
    { read-request boolean } ;
    
: <port-integer> ( smart-integer port -- port-integer )
                           #! smart-integer
    [ active-node>> ]      #! active-node
    [ read-request>> ] bi  #! read-request
    port-integer boa ;

ERROR: reading-not-connected-port-error ;
ERROR: deadlock-reading-error ;
ERROR: no-port-value-error ;
ERROR: get-port-value-error ;

:: get-port-value-and-set-history ( port nd -- value/f )
    port dup
    [ port active-node>> -1 = [ reading-not-connected-port-error ] when
      port [ active-node>> nd = not ] [ read-request>> ] bi and
      [ port port>nds-array "Deadlock-Reading-Nodes"
        dbg-set deadlock-reading-error ] when
      port p-value-> [ no-port-value-error ] unless
      values-history-> port [ p-value-> ] [ <port-integer> 1vector ] bi append
      dup length 30 > [ rest ] when
      port values-history<-
      port read-count-inc
      port p-value->
!     port reset-port
    ] [ get-port-value-error ] if ;

#!
#! PORT NUMBERS
#!

: -d-u ( -- n ) HEX: 105 ;
: -d-- ( -- n ) HEX: 115 ;  #! 277
: -dlu ( -- n ) HEX: 125 ;
: -dl- ( -- n ) HEX: 135 ;
: data ( -- n ) HEX: 141 ;
: ---u ( -- n ) HEX: 145 ;  #! 325
: io   ( -- n ) HEX: 15d ;
: --lu ( -- n ) HEX: 165 ;
: --l- ( -- n ) HEX: 175 ;  #! 373
: rd-u ( -- n ) HEX: 185 ;
: rd-- ( -- n ) HEX: 195 ;
: rdlu ( -- n ) HEX: 1a5 ;
: rdl- ( -- n ) HEX: 1b5 ;
: r--u ( -- n ) HEX: 1c5 ;
: r--- ( -- n ) HEX: 1d5 ;  #! 469
: r-lu ( -- n ) HEX: 1e5 ;
: r-l- ( -- n ) HEX: 1f5 ;

: r ( -- n ) BIN: 1000 ;
: d ( -- n ) BIN: 0100 ;
: l ( -- n ) BIN: 0010 ;
: u ( -- n ) BIN: 0001 ;

: valid-port? ( port# -- flag )
    HEX: 105 [ bitand ] keep = ;

: rdlu>port ( seq -- port# )
    BIN: 0101 swap [ bitxor ] each
    4 shift HEX: 105 bitxor ;

: port>rdlu ( port# -- seq/f )
    dup valid-port? [
        -4 shift BIN: 1111 bitand BIN: 0101 bitxor
        ${ r d l u } swap [ bitand ] curry map [ 0 > ] filter ]
    [ drop f ] if ;

#!
#! CONNECT
#!

: rl-direction ( d col -- direction )
    even?
        [ 0 < [ l ] [ r ] if ]
        [ 0 < [ r ] [ l ] if ]
    if ;

: du-direction ( d row -- direction )
    even?
        [ 0 < [ u ] [ d ] if ]
        [ 0 < [ d ] [ u ] if ]
    if ;

ERROR: nodes-not-connected ;

: connect>direction ( nd1 nd2 -- direction/f )
    [ nd>rc 2array ] bi@
    2dup [ first ] bi@ =
    [ 2dup [ second ] bi@ - dup abs 1 =
        [ neg nip swap second rl-direction ] [ 3drop f ] if
    ]
    [ 2dup [ second ] bi@ =
        [ 2dup [ first ] bi@ - dup abs 1 =
        [ neg nip swap first du-direction ] [ 3drop f ] if ]
        [ 2drop f ]
        if
    ] if ;

: connect ( nd1 nd2 -- port#/f )
    2dup 2array "Not-Connected-Nodes" dbg-set
    connect>direction dup
    [ 1array rdlu>port ] [ nodes-not-connected ] if
    f "Not-Connected-Nodes" dbg-set ;

#!
#! GETTING AND SETTING PORTS
#!

: correct-direction ( direction row col -- direction' )
    pick ${ r l } member?
    [ nip BIN: 1010 ] [ drop BIN: 0101 ] if
    swap odd? [ bitxor ] [ drop ] if ;

ERROR: invalid-direction-in-get-prt ;

:: get-prt ( direction nd cols -- prt )
    cols 1 +
    nd cols /mod :> ( cols' row col )
    direction row col correct-direction
    {
      { $ r [ 2 row * 1 + cols' * col + row - ] }
      { $ l [ 2 row * 1 + cols' * col + row - 1 - ] }
      { $ d [ 2 row * 2 + cols' * col + row - 1 - ] }
      { $ u [ 2 row * cols' * col + row - ] }
      [ f invalid-direction-in-get-prt ]
    } case ;

: get-port ( direction nd -- port/f )
    this-chip
    [ type>> get-cols get-prt ]
    [ ports>> ] bi nth ;

: set-port ( port direction nd -- )
    this-chip
    [ type>> get-cols get-prt ]
    [ ports>> ] bi set-nth ;

: nds>prt ( nd1 nd2 -- prt )
    2dup 2array "Not-Connected-Nodes" dbg-set
    2dup connect>direction dup
    [ nip swap this-chip type>> get-cols get-prt ]
    [ 2drop nodes-not-connected ] if
    f "Not-Connected-Nodes" dbg-set ;

#!
#! PORTNUMBER TO NSEW
#!

: rdlu>nsew-char ( n -- nsew-char )
    "-nw-s---e" nth 1string ; inline

:: rdlu>nsew ( n nd -- nsew-char )
    nd nd>rc :> ( row col )
    n [ 1 = ] [ 4 = ] bi or
    [ row even? [ n 1 = [ 4 ] [ 1 ] if ] [ n ] if ]
    [ col even? [ n ] [ n 2 = [ 8 ] [ 2 ] if ] if ]
    if rdlu>nsew-char ;

:: sort-nsew ( seq -- seq' )
    V{ } clone
    { "n" "s" "e" "w" } [ dup seq member? [ swap [ push ] keep ]
    [ drop ] if  ] each ;

:: port#>nsew ( nd port# -- nsew-string )
    port# port>rdlu [ nd rdlu>nsew ] map
    sort-nsew "" join ;

#!
#! MOVE FROM NODE TO NODE
#!

: nd>left ( nd -- nd'/f ? )
    nd>rc 1 - dup 0 >= [ [ this-chip type>> get-cols * ] dip
    + t ] [ 2drop f f  ] if ;

: nd>right ( nd -- nd'/f ? )
    nd>rc 1 + dup this-chip type>> get-cols <
    [ [ this-chip type>> get-cols * ] dip + t ] [ 2drop f f  ]
    if ;

: nd>down ( nd -- nd'/f ? )
    nd>rc [ 1 + ] dip over this-chip type>> get-rows <
    [ [ this-chip type>> get-cols * ] dip + t ] [ 2drop f f  ]
    if ;

: nd>up ( nd -- nd'/f ? )
    nd>rc [ 1 - ] dip over 0 >=
    [ [ this-chip type>> get-cols * ] dip + t ] [ 2drop f f  ]
    if ;

: nd-in-direction ( nd direction -- nd'/f ? )
    over nd>rc correct-direction
    { { r [ nd>right ] }
      { l [ nd>left ] }
      { d [ nd>down ] }
      { u [ nd>up ] }
      [ 2drop f f ]
    } case ;

ERROR: invalid-node-pair ;

:: port#>nd-pairs ( nd port# -- nd-pairs )
    port# port>rdlu
    [ nd swap nd-in-direction [ invalid-node-pair ] unless
      nd 2array ] map ;

#!
#! PORTS
#!

: port#? ( addr -- ? )
    [ HEX: 1f5 <= ] [ HEX: 105 [ bitand ] keep = ] bi and ;

SYMBOLS: Create-Ports-Nd Create-Ports-Direction ;

:: (create-ports) ( direction nd -- )
    nd Create-Ports-Nd set
    direction Create-Ports-Direction set
    direction nd get-port
    [ nd dup direction nd-in-direction drop <port>
      direction nd set-port ]
    unless ;

: create-ports ( rdlu nd -- )
    [ (create-ports) ] curry each ; inline

: maybe-create-ports ( addr nd -- ? )
    over port#? [ [ port>rdlu ] dip create-ports t ] [ 2drop f ] if ;

:: this-node-activated-port? ( directions nd -- ? )
    directions [ nd get-port dup [ nd swap =active-node? ] when ] any? ;

: activated-port? ( directions nd -- ? )
    [ get-port dup [ active-node? ] when ] curry any? ;

:: neighbour-reading? ( directions nd -- ? )
    directions
    [ nd get-port dup
        [ [ [ active-node>> nd = not ] [ read-request>> ] bi and ]
          [ [ active-node>> nd = ] [ write-request>> ] [ request-accepted>> ] tri and and ]
          bi or
        ] when
    ] any? ;

:: neighbour-writing? ( directions nd -- ? )
    directions
    [ nd get-port dup
        [ [ [ active-node>> nd = not ] [ write-request>> ] bi and ]
          [ [ active-node>> nd = ] [ read-request>> ] [ request-accepted>> ] tri and and ]
          bi or
        ] when
    ] any? ;

: change-active-nodes-in-port ( directions nd -- )
    [ [ swap ] [ get-port ] bi dup [ active-node<< ] [ 2drop ] if ] curry each ;

: read-from-port ( directions nd -- data )
    [ [ get-port ] [ get-port-value-and-set-history ] bi ] curry map ?first ;

:: write-to-port ( data directions nd -- )
    directions [ data swap nd [ get-port ] [ set-port-value-from-node ] bi ] each ;

: any-port-accepted-request? ( directions nd -- ? )
    [ get-port dup [ request-accepted>> ] when ] curry any? ;

: init-connected-ports (  directions nd -- )
    [ get-port reset-port ] curry each ;

: init-other-ports ( directions nd -- )
    [ get-port dup
      request-accepted>> [ drop ] [ reset-port ] if
    ] curry each ;

: request-read-from-port ( directions nd -- )
    '[ t swap _ get-port read-request<< ] each ;

: request-write-to-port ( directions nd -- )
    '[ t swap _ get-port write-request<< ] each ;

: request-accepted-by-port ( directions nd -- )
    '[ t swap _ get-port request-accepted<< ] each ;

#! FETCH

: port@ ( -- port )
    Port get ;
