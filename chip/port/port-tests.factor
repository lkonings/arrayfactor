#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: arrayfactor.chip.node arrayfactor.chip.port
accessors arrays combinators hashtables kernel literals locals math
namespaces prettyprint sequences tools.test words words.symbol ;

IN: arrayfactor.chip.port.tests

[ 501 ] [ ${ r l } rdlu>port ] unit-test

#!  "arrayfactor.chip.port" test
