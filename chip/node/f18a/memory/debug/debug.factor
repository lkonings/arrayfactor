! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays fry hashtables
kernel math namespaces sequences vectors ;
IN: arrayfactor.chip.node.f18a.memory.debug

#! THIS CODE IS NOT USED YET!

TUPLE: debug
    { break-points vector } ;

: <debug> ( -- debug )
    V{ } clone         #! break-points
    debug boa ;

: add-break-point ( addr debug -- )
    break-points>> push ; inline

: remove-break-point ( addr debug -- )
    swap '[ [ _ = not ] filter ] change-break-points drop ;

: clear-break-points ( debug -- )
    [ drop V{ } clone ] change-break-points drop ;

: has-break-point ( addr debug -- ? )
    break-points>> member? ; inline

TUPLE: break ;

: <break> ( -- break )
    break boa ;

! USE: arrayfactor.chip.node.f18a.memory.debug
