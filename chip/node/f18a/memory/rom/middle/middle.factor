! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors hashtables kernel namespaces sequences vectors ;
IN: arrayfactor.chip.node.f18a.memory.rom.middle

: MIDDLE-ROM ( -- seq )
V{
    V{ HEX: 04bb2 }
    V{ HEX: 00115 }
    V{ HEX: 05b02 }
    V{ HEX: 039b2 }
    V{ HEX: 26b25 }
    V{ HEX: 134aa }
    V{ HEX: 043e2 }
    V{ HEX: 20400 }
    V{ HEX: 0bd2a }
    V{ HEX: 05a4d }
    V{ HEX: 26b02 }
    V{ HEX: 04bb2 }
    V{ HEX: 00115 }
    V{ HEX: 05b25 }
    V{ HEX: 056ab }
    V{ HEX: 04b02 }
    V{ HEX: 0015d }
    V{ HEX: 3646d }
    V{ HEX: 2efb2 }
    V{ HEX: 135d5 }
    V{ HEX: 20cb2 }
    V{ HEX: 040b2 }
    V{ HEX: 00800 }
    V{ HEX: 194db }
    V{ HEX: 2e9b2 }
    V{ HEX: 00800 }
    V{ HEX: 194db }
    V{ HEX: 2e9b2 }
    V{ HEX: 13545 }
    V{ HEX: 269b2 }
    V{ HEX: 3b7cf }
    V{ HEX: 134a9 }
    V{ HEX: 134a9 }
    V{ HEX: 134a9 }
    V{ HEX: 134a9 }
    V{ HEX: 134a9 }
    V{ HEX: 134a9 }
    V{ HEX: 134a9 }
    V{ HEX: 134a9 }
    V{ HEX: 134a9 }
    V{ HEX: 134a9 }
    V{ HEX: 115a5 }
    V{ HEX: 248c2 }
    V{ HEX: 307c2 }
    V{ HEX: 307c2 }
    V{ HEX: 307c2 }
    V{ HEX: 1b4b9 }
    V{ HEX: 04092 }
    V{ HEX: 07fff }
    V{ HEX: 043b2 }
    V{ HEX: 18000 }
    V{ HEX: 0bd2a }
    V{ HEX: 05a48 }
    V{ HEX: 0409a }
    V{ HEX: 06000 }
    V{ HEX: 04055 }
    V{ HEX: 003ff }
    V{ HEX: 201b2 }
    V{ HEX: 114af }
    V{ HEX: 134aa }
    V{ HEX: 043e2 }
    V{ HEX: 28400 }
    V{ HEX: 0bd2a }
    V{ HEX: 05a4b }
} ;
