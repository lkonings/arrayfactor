! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays hashtables kernel
math models namespaces sequences vectors
arrayfactor.chip
arrayfactor.chip.node.codes
arrayfactor.chip.node.f18a.execution.time
arrayfactor.chip.node.f18a.memory.debug
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.memory.rom.basic
arrayfactor.core.utils ;
IN: arrayfactor.chip.node.f18a.memory.rom

TUPLE: rom < ram ;

SYMBOL: Rom

: <basic-rom> ( -- rom )
    !TODO implement other roms.
    BASIC-ROM
    dup disassembly                    #! coding
    dup clone                          #! code
    rot* [ first ] map                  #! binary
    over code-seq>timed-code           #! timed-code
    [ <model> ] tri@
    BASIC-NAMES <model>                #! names
    { } clone <model>                  #! addr-list
    <debug>                            #! debug
    t                                  #! notify-connections
    rom boa
    dup Rom set
    dup chip-contains-models ;

: <rom> ( nd -- rom )
    drop <basic-rom> ;

#!
#! ADDRESSING ROM
#!

: rom-addr? ( addr -- ? )
    [ HEX: 80 >= ] [ HEX: ff <= ] bi and ; inline

: rom-addr ( addr -- addr' )
    HEX: 3f bitand HEX: 80 + ; inline

: addr>rom-addr ( addr -- rom-addr/f )
    dup rom-addr? [ mask-addr ] [ drop f ] if ;

: rom-addr-inc ( rom-addr -- rom-addr' )
    1 + rom-addr ;

#! FETCH

: rom@ ( -- type )
    Rom get ;
