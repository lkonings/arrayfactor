! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.core.utils hashtables kernel
namespaces sequences vectors ;
IN: arrayfactor.chip.node.f18a.memory.rom.basic

: BASIC-ROM ( -- seq )
V{
    V{ HEX: 26aba }
    V{ HEX: 0ecb0 }
    V{ HEX: 270bd }
    V{ HEX: 02a55 }
    V{ HEX: 248ba }
    V{ HEX: 209b2 }
    V{ HEX: 311aa }
    V{ HEX: 3e81b }
    V{ HEX: 066b0 }
    V{ HEX: 32cab }
    V{ HEX: 243b2 }
    V{ HEX: 351ba }
    V{ HEX: 3acb0 }
    V{ HEX: 15555 }
    V{ HEX: 04f69 }
    V{ HEX: 10000 }
    V{ HEX: 3a9f5 }
    V{ HEX: 3a6b0 }
    V{ HEX: 33555 }
    V{ HEX: 24de3 }
    V{ HEX: 2c1ed }
    V{ HEX: 136d3 }
    V{ HEX: 2bdba }
    V{ HEX: 00011 }
    V{ HEX: 249f2 }
    V{ HEX: 2edb0 }
    V{ HEX: 24eb0 }
    V{ HEX: 1b6de }
    V{ HEX: 3ac78 }
    V{ HEX: 249f5 }
    V{ HEX: 203e2 }
    V{ HEX: 270d8 }
    V{ HEX: 249f5 }
    V{ HEX: 26a1a }
    V{ HEX: 2fc7c }
    V{ HEX: 3b7a8 }
    V{ HEX: 26fbf }
    V{ HEX: 236a1 }
    V{ HEX: 09b22 }
    V{ HEX: 07b72 }
    V{ HEX: 228ad }
    V{ HEX: 115b5 }
    V{ HEX: 26aba }
    V{ HEX: 06eb2 }
    V{ HEX: 2f6b7 }
    V{ HEX: 26a18 }
    V{ HEX: 230ac }
    V{ HEX: 2f555 }
    V{ HEX: 2bdbb }
    V{ HEX: 00010 }
    V{ HEX: 243b2 }
    V{ HEX: 351c9 }
    V{ HEX: 232b6 }
    V{ HEX: 3a6dd }
    V{ HEX: 3a4cd }
    V{ HEX: 134b0 }
    V{ HEX: 2246b }
    V{ HEX: 3a6da }
    V{ HEX: 33555 }
    V{ HEX: 3a455 }
    V{ HEX: 26aba }
    V{ HEX: 07eba }
    V{ HEX: 228b2 }
    V{ HEX: 134b0 }
} ;

CONSTANT: BASIC-NAMES
H{
    { "relay"      HEX: 21 } ! a1
    { "warm"       HEX: 29 } ! a9
    { "poly"       HEX: 2a } ! aa
    { "*.17"       HEX: 30 } ! b0
    { "*."         HEX: 37 } ! b7
    { "taps"       HEX: 3c } ! bc
    { "interp"     HEX:  4 } ! c4
    { "triangle"   HEX:  e } ! ce
    { "-u/mod"     HEX: 13 } ! d3
}
