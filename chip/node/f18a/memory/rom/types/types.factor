! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors kernel namespaces sequences vectors ;
IN: arrayfactor.chip.node.f18a.memory.rom.types

: analog-roms ( -- seq )
    { 117 617 709 713 717 } ;

: serdes-roms ( -- seq )
    { 1 701 } ;

: spi-roms ( -- seq )
    { 705 } ;

: async-roms ( -- seq )
    { 708 } ;

: sync-roms ( -- seq )
    { 300 } ;

: 1wire-roms ( -- seq )
    { 200 } ;

! =============

: data-roms ( -- seq )
    { 7 } ;

: control-roms ( -- seq )
    { 8 } ;

: address-roms ( -- seq )
    { 9 } ;

! USE: arrayfactor.chip.node.f18a.memory.rom.types
