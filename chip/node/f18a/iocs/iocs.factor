#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: arrayfactor.chip.node
accessors arrays assocs combinators kernel locals math
namespaces sequences words words.symbol ;
IN: arrayfactor.chip.node.f18a.iocs

TUPLE: iocs
     { write array }
     { read array } ;
