! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip.node.f18a.compilation.cfs
arrayfactor.chips arrayfactor.core.preferences
io.encodings.utf8 io.files io.pathnames kernel locals math
math.parser sequences splitting ;
IN: arrayfactor.chip.node.f18a.compilation.cfs.export

CONSTANT: header
"\ $Id: parpi.cfs,v 1.15 2011/02/04 16:51:25 leon Exp $
\ Copyright (2010): Albert van der Horst {by GNU Public License}

\ The parallel Pi program: parpi
\ Parallel program for counting primes on the Green Arrays chips.

\ Intended to be compiled by a special ciasa (arrayforth)
\ that has colornames burnt in.

0  ORG\n\n"

: format-source ( source -- source' )
    " " split
    [ dup "]:" = [ drop "\n]:" ] when ] map
    " " join ;

:: screen-header ( block -- string )
    { "( SCREEN) #" block " 400 * DUP SEGMENT SCR" block "    EDIT-TIME" "\n" } "" join ;

: screen-footer ( -- string )
    "\n\n    PREVIOUS\n" ;

: cfs-node ( node block -- string )
    number>string screen-header
    swap compilation>> source>> source>cfs
    " " join format-source append
    screen-footer append ;

: export-cfs ( start-block -- )
    header
    this-chip nodes>> sift [ pick cfs-node [ 1 + ] 2dip ] map "" join
    append
    fdir "reports/cfs/cfs-export.txt" append-path utf8 set-file-contents
    #! Get rid of start-block'
    drop ;

#! USE: arrayfactor.chip.node.f18a.compilation.cfs.export
