! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.node.f18a.status arrayfactor.chip.nodes
arrayfactor.chip.port arrayfactor.chip.smart-integer
arrayfactor.chips arrayfactor.core.preferences
arrayfactor.core.utils arrays combinators io io.encodings.utf8
io.files io.pathnames kernel locals math math.parser namespaces
sequences vectors ;
IN: arrayfactor.chip.node.f18a.compilation.step-report

! : write ( string -- ) .. ;

: ?char-write ( char ? -- ) [ drop "-" ] unless write ;

: active-port-write ( port# -- )
    dup port#?
    [ port>rdlu dup empty?
      [ drop "-" write ]
      [ first this-node nd>> get-port dup
        [ active-node? ">" swap ?char-write ]
        [ drop "-" write ]
        if
    ] if ] [ drop "-" write ] if ;

: request-accepted-write ( port# -- )
    dup port#?
    [ port>rdlu dup empty?
      [ drop "-" write ]
      [ this-node nd>> any-port-accepted-request?
        "a" swap ?char-write
      ] if
    ] [ drop "-" write ] if ;

: line-write ( string -- ) "\n" append write ;

: opcode>string ( vector -- string )
    first dup
    [ dup vector?
      [ [ first number>string ] [ second ] bi 2array " " join ]
      [ dup number? [ number>string ] when ]
      if
    ] [ drop "f" ] if ;

: opcode-write ( seq -- ) opcode>string "{ " prepend " }" append write ;

: stack-write ( seq -- ) [ >hex 5 zeros-pad-numeric-string ] map " " join write ;

: blocking-write ( boolean -- ) [ "*" ] [ " " ] if write ;

: space-write ( -- ) " " write ;

: node>write-one-line ( nd -- )
  this-chip node!
  this-chip step>> number>string 4 spaces-pad-numeric-string write space-write
  this-node {
    [ get-stacks {ds} stack-write space-write ]
    [ "a:" write a> n> >hex 5 zeros-pad-numeric-string write space-write ]
    [ a> n> [ active-port-write ] [ request-accepted-write ] bi space-write ]
    [ "b:" write b> n> >hex 5 zeros-pad-numeric-string write space-write ]
    [ b> n> [ active-port-write ] [ request-accepted-write ] bi space-write ]
    [ "r:" write r> n> >hex 5 zeros-pad-numeric-string write space-write ]
    [ "p:" write p> >hex 5 zeros-pad-numeric-string write space-write ]
  } cleave
  this-node
    [ opcode-> opcode-write ]
    [ status>> blocking-> blocking-write ]
  bi
  "" line-write ;

: node-name ( node# -- name ) number>string "00" prepend 3 tail* "node_" prepend ".txt" append ;

: step-report-path ( node#name -- path ) fdir "reports/steps" append-path prepend-path  ;

SYMBOL: Node-pathes-list

: node-pathes-list ( -- list )
    this-chip get-nodes [ dup [ node#>> node-name step-report-path ] when ] map ;

: start-report-stepping ( -- )
    start-stepping
    node-pathes-list Node-pathes-list set ;

:: (report-step) ( nd path -- )
    path utf8 [ nd node>write-one-line ] with-file-appender ;

: report-step ( -- )
   1 steps 
   Node-pathes-list get [ over [ swap (report-step) ] [ 2drop ] if ] each-index ;

: report-steps ( n -- ) [ report-step ] times ;

! USE: arrayfactor.chip.node.f18a.compilation.step-report
