! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrayfactor.chip
arrayfactor.chip.models arrayfactor.chip.node
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.memory
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.node.f18a.status
arrayfactor.chip.smart-integer arrayfactor.chips kernel math
models namespaces sequences strings ;
FROM: arrayfactor.chip.node => node ;
IN: arrayfactor.chip.node.f18a

TUPLE: f18a < node
    { name string }
    { node# integer }
    { nd integer }
    { memory memory }
    { instruction-stack model }
    { opcode model }
    { a model }
    { b model }
    { io integer }
    { stacks stacks }
    { p model }
    { address integer }
    { data integer }
    { number integer }
    { status status }
    { assembly assembly }
    { notify-connections boolean } ;

M: f18a node-init ( nd compilation node -- node )
    swap                          >>compilation
    ""                            >>name
    over nd>node#                 >>node#
    over                          >>nd
    swap nd>memory                >>memory
    V{ } clone <model>            >>instruction-stack
    V{ } clone <model>            >>opcode
    0 <smart-integer> <model>     >>a
    0 <smart-integer> <model>     >>b
    0                             >>io
    <stacks>                      >>stacks
    0 <model>                     >>p
    0                             >>address
    0                             >>data
    0                             >>number
    <status>                      >>status
    dup memory>> <assembly>       >>assembly
    t                             >>notify-connections
    dup chip-contains-models ;
    
: data>memory ( data addr node -- )
    memory>> (ram|rom) data>ram ;

: memory>data ( addr node -- data )
    memory>> memory-get-binary-item ;

: instruction-stack-model-> ( node -- model )
    instruction-stack>> ; inline

: instruction-stack-> ( node -- vector )
    instruction-stack-model-> value>> ;

: instruction-stack<- ( vector node -- )
    dup instruction-stack-model-> set-model** ; inline

: ->instruction-stack ( node vector -- node )
    over instruction-stack<- ; inline

: clear-instruction-stack ( node -- )
    dup instruction-stack-model-> V{ } clone -rot set-model** ;

: opcode-model-> ( node -- opcode )
    opcode>> ;

: opcode-> ( node -- opcode )
    opcode-model-> value>> ;

: opcode<- ( opcode node -- )
    dup opcode-model-> set-model** ; inline

: ->opcode ( node opcode -- node )
    over opcode<- ; inline

: get-stacks ( node -- stacks )
    stacks>> ;

: set-node-name ( node name -- )
    >>name drop ;

: get-node-name ( node -- name )
    name>> ;

#!
#! PROGRAM COUNTER
#!

#! point p to next addr in memory,
#!   or same addr in port.
: p-inc?! ( node -- )
    !TODO correct mask!
    dup p>> [ nip value>> addr-inc ] [ set-model** ] 2bi ;

#! point p to next addr in memory
: p-inc ( node -- )
    !TODO correct mask!
    dup p>> [ nip value>> 1 + ] [ set-model** ] 2bi ;

: p-dec ( node -- )
    !TODO correct mask!
    dup p>> [ nip value>> 1 - ] [ set-model** ] 2bi ;

: p-model> ( node -- model )
    p>> ; inline

: p> ( node -- p )
    p-model> value>> ; inline

: p< ( addr node -- )
    dup p-model> set-model** ;

ALIAS: >p p<

: >p+ ( addr node -- )
    [ 1 + ] dip >p ;

#!
#! A
#!

: a-model> ( node -- model )
    a>> ; inline

: a> ( node -- smart-integer  )
    a-model> value>> ; inline

: >a ( smart-integer node -- )
    dup a-model> set-model** ; inline

ALIAS: a< >a

: a-inc?! ( node -- )
    dup a-model>
    [ nip value>> n> addr-inc <smart-integer> ] [ set-model** ]
    2bi ;

#!
#! B
#!

: b-model> ( node -- model )
    b>> ; inline

: b> ( node -- smart-integer )
    b-model> value>> ; inline

: >b ( smart-integer node -- )
    dup b-model> set-model** ; inline

ALIAS: b< >b

: push-f-back ( node -- )
    f swap instruction-stack-> push ; inline

#!
#! THIS - objects belonging to current (this) node
#!

: this-node ( -- node )
    this-chip this-node-> ; inline

: this-ram ( -- ram )
    this-node memory>> ram>> ;

: this-rom ( -- rom )
    this-node memory>> rom>> ;

: this-assembly ( -- assembly )
    this-node assembly>> ;

: this-compilation ( -- compilation )
    this-node compilation>> ;

#!
#! Standard-Node
#!

SYMBOL: Standard-Node

: set-standard-node ( -- )
    99999 <node> Standard-Node set-global ;

: is-standard-node#? ( node# -- ? )
    99999 = not ; inline

: is-real-node? ( node/f -- ? )
    dup node? [ node#>> is-standard-node#? ] when ;

: is-real-node#? ( node# -- ? )
    node#>node >boolean ; inline

! "arrayfactor.chip.node.f18a.execution" require

