! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip arrayfactor.chip.chip-type
arrayfactor.chip.node arrayfactor.chip.node.codes
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.node.f18a.memory
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.node.f18a.status
arrayfactor.chip.nodes arrayfactor.chip.port
arrayfactor.chip.smart-integer arrayfactor.chips kernel logging
make math namespaces quotations sequences tools.test vectors ;
IN: arrayfactor.chip.node.f18a.execution.tests

: e#  ( n -- )
    <smart-integer> this-node # ;

: e#> ( -- n )
    this-node #> n> ;


<ga144> <chip> drop

[ 5 ]
[ 5 <node> drop this-node nd>> ]
unit-test

#! SWAP- + 1
! [ V{ 87381 87381 87381 87381 87381 87381 87381 87381 87381 9 } ]
! [ 2 <node> drop 3 e# 12 e# this-node ,- this-node ,+ this-node ,- this-node get-stacks {ds} ]
! unit-test

! [ 6 123 ]
! [ 2 <node> drop
!   5 e# this-node ,a!
!   123 e# this-node ,!
!   this-node ,a e#>
!   dup 1 - this-node addr>data n>
! ]
! unit-test

[ V{ 6 7 8 9 10 11 12 13 14 15 } ]
[ 2 <node> drop 0 15 [ 1 + dup e# ] times drop this-node get-stacks {ds} ]
unit-test

[ V{ 13 6 7 8 9 10 11 12 13 14 } ]
[ 2 <node> drop 0 15 [ 1 + dup e# ] times drop this-node ,drop this-node get-stacks {ds} ]
unit-test

[ V{ 8 9 10 11 12 13 14 15 14 14 } ]
[ 2 <node> drop 0 15 [ 1 + dup e# ] times drop this-node ,over this-node ,dup this-node get-stacks {ds} ]
unit-test

[ V{ 12 13 6 7 8 9 10 11 12 13 } V{ 14 13 12 11 10 9 8 7 6 } ]
[ 2 <node> drop 0 15 [ 1 + dup e# ] times drop 10 [ this-node ,push ] times this-node [ get-stacks {ds} ] [ get-stacks {rs} ] bi ]
unit-test

[ V{ 10 11 12 13 6 7 8 9 10 11 } V{ 12 11 10 9 8 7 14 13 12 } ]
[ 2 <node> drop 0 15 [ 1 + dup e# ] times drop
  10 [ this-node ,push ] times 6 [ this-node ,pop ] times this-node [ get-stacks {ds} ] [ get-stacks {rs} ] bi ]
unit-test

[ 15 14 V{ 12 13 6 7 8 9 10 11 12 13 } ]
[ 2 <node> drop 0 15 [ 1 + dup e# ] times drop e#> e#> this-node get-stacks {ds} ]
unit-test

[ 5 ] [ 2 <node> drop 5 e# this-node ,a! this-node ,a e#> ] unit-test
! [ 5 ] [ 2 <node> drop 5 e# this-node ,b! this-node b> n> ] unit-test

[ t ]
[ 7 <smart-integer>
  "name3" swap smart-integer-action
  "name3" this-chip get-named-smart-integers name>smart-integer? nip
]
unit-test

remove-chip

#! "arrayfactor.chip.node.f18a.execution" test
