#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: kernel
arrayfactor.chip.chip-type
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.compilation ;
IN: arrayfactor.chip.node.f18a.ga144

TUPLE: ga144 < chip-type ;

: <ga144> ( -- ga144 )
    8 18 [ <compilation> ] f18a ga144 boa ;
