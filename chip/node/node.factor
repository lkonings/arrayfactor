! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip arrayfactor.chips kernel
models sequences ;
IN: arrayfactor.chip.node

TUPLE: node compilation ;

GENERIC: node-init ( nd node-class compilation -- node ) 

: compilation-constructor ( nd chip-type -- compilation )
    [ nd>node# ] dip
    compilation-constructor>> call( node# -- compilation ) ;

: compilation<node ( node -- node )
    dup dup compilation>> node<<  ; inline

: chip-node ( nd -- node )
    dup this-chip type>>
    [ compilation-constructor ]
    [ node-class>> new ] bi                
    node-init
    compilation<node ;

: <node> ( node# -- node )
    node#>nd chip-node
    dup this-chip this-node-model-> set-model ;
 
: {node} ( node# -- )
    [ <node> ] [ node#>nd ] bi this-chip
    nodes>> set-nth ;
