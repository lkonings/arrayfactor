! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: arrayfactor.chip.node.codes tools.test namespaces sequences kernel quotations vectors ;
IN: arrayfactor.chip.node.codes.tests

[ V{
    V{ "over" }
    V{ 5 "jmp" }
    V{ "dup" }
    V{ 6 "call" }
    V{ 3 }
    V{ "." }
} ]
[ "over 5 jmp dup 6 call 3 ." source>code ]
unit-test

[ V{
    V{ "over" }
    V{ f "jmp" }
    V{ "dup" }
    V{ 6 "call" }
    V{ 3 }
    V{ "." }
} ]
[ "over jmp dup 6 call 3 ." source>code ]
unit-test

#! "arrayfactor.chip.node.codes" test
