! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: arrayfactor arrayfactor.chip.nodes arrayfactor.ui
arrayfactor.ui.console arrayfactor.ui.console.instructions
arrayfactor.ui.console.windows kernel ;
IN: arrayfactor.projects.circular-stack

&&chip

100 {node

{ 8 7 6 5 4 3 2 1 0 -1 } &table

": init [ 200 ] cn b! " &
": fill-stack 0 a! 9 for @+ unext . ." &
": pump drop over !b pump" &
"[ 169 org ] init" &

node}

200 {node

": init [ 100 ] cn a! . ." &
": receive @a receive" &
"[ 169 org ] init" &

node}

init-ui
start-stepping

&&4nodes

! USE: arrayfactor.projects.circular-stack
