#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays assocs combinators fry grouping kernel locals logging
math math.ranges namespaces sequences words words.symbol
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.node
arrayfactor.chip.connection
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation.cfs
arrayfactor.chip.node.f18a.compilation
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chips
arrayfactor.core.library.macros.basic
arrayfactor.core.macros
arrayfactor.chip.port
arrayfactor.core.utils ;
IN: arrayfactor.projects.parpi

: ld ( relative-path node# -- )
  {node}
  this-compilation get-cfs swap read-source
  this-compilation get-cfs translate-source ;

: (multiple-connections) ( nd1 nd2 -- seq/f )
    [ nd>rc 2array ] bi@
    2dup [ first ] bi@ =
    [ dup first 100 * '[ second _ + ] bi@ [a,b] [ ] map ]
    [ 2dup [ second ] bi@ =
      [ dup second '[ first 100 * _ + ] bi@ 100 <range> [ ] map ]
      [ 2drop f ]
      if
    ] if ;

ERROR: multiple-connections-error ;

: multiple-connections ( node#1 node#2 -- seq/f )
    [ node#>nd ] bi@ (multiple-connections)
    dup [ multiple-connections-error ] unless ;

: set-connected-nodes ( connection seq -- ) { [ first prvnd ] [ second prvnd1 ] [ third nxtnd ] [ fourth nxtnd1 ] } 2cleave ;

: (lds) ( start-at-word relative-path 2nodes -- ) rot* drop
    this-node node#>> -rot*
    [ first ld ] keep
    this-compilation connection>> [ rot* prvnd ] [ swap second nxtnd ] bi ;

ERROR: start-at-word-error ;

:: (lds)* ( start-at-word relative-path 2nodes -- )
  ! start-at-word relative-path 2nodes (lds)
    this-node node#>>
    relative-path 2nodes first ld
    this-compilation connection>> [ swap prvnd ] [ 2nodes second nxtnd ] bi
    this-compilation dup get-cfs new-source>> " " join compile-source
    this-compilation get-source this-assembly assemble-source
    this-assembly end-assembly
    start-at-word this-ram name>addr [ start-at-word-error ] unless this-assembly replace-a9-code ;

: lds ( start-at-word relative-path node#1 node#2 -- )
    multiple-connections 2 clump [ 3dup (lds)* drop ] each 2drop ;

: ldss ( start-at-word relative-path route -- )
    2 clump [ 3dup [ first ] [ second ] bi lds drop ] each 2drop ;

:: compile+assemble-one ( cfs-file node-name node# start-at-word seq -- )
    cfs-file node# ld
    this-node node-name set-node-name
    this-compilation connection>> seq set-connected-nodes
    this-compilation dup get-cfs new-source>> " " join compile-source
    this-compilation get-source this-assembly assemble-source
    this-assembly end-assembly
    start-at-word this-ram name>addr [ start-at-word-error ] unless this-assembly replace-a9-code ;

:: compile+assemble-many ( cfs-file route start-at-word -- )
    start-at-word cfs-file route ldss ;

: control-node ( -- ) "projects/parpi/cfs/218.cfs" "control" 0 "init" { -1 -1 1 -1 } compile+assemble-one ;

: generator-node ( -- ) "projects/parpi/cfs/220.cfs" "generator-0" 1 "init" { 0 -1 2 101 } compile+assemble-one ;

: generator1-node ( -- ) "projects/parpi/cfs/220.cfs" "generator-1" 101 "init" { 1 -1 102 201 } compile+assemble-one ;

: early-sieve-nodes ( -- )
    1 >current-node
    "projects/parpi/cfs/222.cfs" { 8 2 } reverse "init" compile+assemble-many ;

: early-sieve1-nodes ( -- )
    101 >current-node
    "projects/parpi/cfs/222.cfs" { 108 102 } reverse "init" compile+assemble-many ;

: pass-node ( -- ) "projects/parpi/cfs/230.cfs" "pass" 108 "init" { 107 -1 8 -1 } compile+assemble-one ;

: merge-node ( -- ) "projects/parpi/cfs/228.cfs" "merge" 8 "init" { 7 108 9 -1 } compile+assemble-one ;

: late-sieve-nodes ( -- )
    8 >current-node
    "projects/parpi/cfs/224.cfs" { 700 717 617 600 500 517 417 400 300 317 217 210 110 117 17 9 }
    reverse "init" compile+assemble-many ;

: result-node ( -- ) "projects/parpi/cfs/226.cfs" "result" 700 "init" { 701 -1 -1 -1 } compile+assemble-one ;

: parpi ( -- )
    <ga144> <chip> drop
    define-macros
    control-node
    generator-node
    generator1-node
    early-sieve-nodes  
    early-sieve1-nodes  
    pass-node
    merge-node
    late-sieve-nodes
    result-node ;

: show-blocking ( -- seq )
    this-chip nodes>> [ dup [ dup status>> blocking-> [ opcode-> first ] [ drop f ] if ] when ] map ;

! USE: arrayfactor.projects.parpi
