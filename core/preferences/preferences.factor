#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors io.directories io.pathnames kernel memory
namespaces sequences strings vocabs.loader ;
IN: arrayfactor.core.preferences

TUPLE: preferences
     { main-path string }
     { image-path string } ;
     
SYMBOL: Preferences

: <preferences> ( -- preferences )
    vocab-roots get last "arrayfactor" append-path #! mainpath
    ""                                             #! image
    preferences boa
    dup Preferences set-global ;

: this-preferences ( -- type )
    Preferences get-global ;

: fdir ( -- main-path )
    this-preferences main-path>> ;

: save-arrayfactor-as ( image -- )
    fdir prepend-path
    [ this-preferences image-path<< ]
    [ save-image ] bi ; 

: save-arrayfactor ( -- )
    "arrayfactor.image" save-arrayfactor-as ;

: clear-arrayfactor ( -- )
    this-preferences image-path>> delete-file ;

<preferences> drop

#! USE: arrayfactor.core.preferences
