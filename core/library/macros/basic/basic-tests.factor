! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip arrayfactor.chip.chip-type
arrayfactor.chip.connection arrayfactor.chip.node
arrayfactor.chip.node.codes arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation
arrayfactor.chip.node.f18a.compilation.cfs
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.node.f18a.memory
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.memory.rom
arrayfactor.chip.node.f18a.stacks arrayfactor.chip.nodes
arrayfactor.chip.smart-integer arrayfactor.chips
arrayfactor.core.library.macros.basic arrayfactor.core.macros
kernel logging math namespaces quotations sequences tools.test
vectors ;
IN: arrayfactor.core.library.macros.basic.tests

: n#  ( n -- ) <smart-integer> this-node # ;
: n#> ( -- n ) this-node #> n> ;

<ga144> <chip> drop

define-macros

[ H{
    { "ge," "over - over +" }
    { "gt," "over over - + -" }
    { "repeat" "end" }
    { "lt," "over - over + -" }
    { "0," "dup dup xor" }
    { "ne," "over over or" }
    { "not" "-" }
    { "double," "dup +" }
    { "le," "over over - +" }
    { "false," "dup dup xor" }
    { "true," "dup dup xor -" }
} ]
[ this-chip macros>> lib>>
] unit-test

[ "0 org : double dup + ; : init double init ;"
  V{ V{ "dup" "+" f ";" } V{ f V{ 0 "call" } } V{ f V{ 1 "jmp" } } }
  V{ 87381 87381 87381 87381 87381 87381 87381 87381 87381 }
  V{ 87381 87381 87381 87381 87381 87381 87381 87381 87381 24 } ]
[ 0 {node} 12 n#
  this-compilation "0 org : double double, ; : init double init ;" compile-source
  this-compilation expanded>> " " join
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  this-ram get-timed-code*
  2 this-assembly replace-a9-code 
  this-chip start-timed-executes
  4 [ this-chip timed-executes ] times
  0 node#>nd this-chip node!
  5 [ this-chip timed-executes ] times
  0 node#>nd this-chip node!
  4 [ this-chip timed-executes ] times
  0 node#>nd this-chip node!
  this-node get-stacks {rs}
  this-node get-stacks {ds}
] unit-test

remove-chip

#! "arrayfactor.core.library.macros.basic" test
