! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrayfactor.chip assocs hashtables
kernel namespaces sequences strings ;
IN: arrayfactor.core.library.macros.basic

: define-macros ( -- )
    "dup +" "double," add-chip-macro
    "over - over +" "ge," add-chip-macro
    "over - over + -" "lt," add-chip-macro
    "over over - +" "le," add-chip-macro
    "over over - + -" "gt," add-chip-macro
    "over over or" "ne," add-chip-macro
    "-" "not" add-chip-macro
    "dup dup xor" "0," add-chip-macro
    "dup dup xor" "false," add-chip-macro
    "dup dup xor -" "true," add-chip-macro
    "end" "repeat" add-chip-macro ;

#! USE: arrayfactor.core.library.macros.basic
