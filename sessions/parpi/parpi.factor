USING: kernel
arrayfactor
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.nodes
arrayfactor.chip.smart-integer
arrayfactor.projects.parpi
arrayfactor.ui
arrayfactor.ui.chip.nodes.navigation
arrayfactor.ui.chip.nodes
arrayfactor.ui.chip.step ;
IN: arrayfactor.sessions.parpi

: run-parpi ( -- )
    parpi
    hardcoded-ram
    init-ui
    start-stepping
    nodes-window
    navigation-window ;

run-parpi

! Nodes-Window get gadgets>> grid>> first first ( viewport ) children>> first ( border ) children>> first ( grid ) children>> first ( border ) children>> first ( node-label-control ) text>>
! Text of node 700 result
! Nodes-Window get gadgets>> grid>> first first children>> first children>> first children>> first children>> first text>>
! 576 borders
! Nodes-Window get gadgets>> grid>> first first children>> first children>> first children>>
! over [ children>> first ] map

! USE: arrayfactor.sessions.parpi
