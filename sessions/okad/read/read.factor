USING: grouping kernel namespaces
io io.binary io.encodings.binary io.files io.pathnames
arrayfactor.chip.node.f18a.execution
arrayfactor.core.preferences
arrayfactor.chip.smart-integer
arrayfactor.projects.parpi
arrayfactor.ui
arrayfactor.chip.node.f18a.compilation.cfs.blocks
arrayfactor.ui.chip.node.f18a.memory
arrayfactor.ui.chip.nodes.navigation
arrayfactor.ui.chip.nodes
arrayfactor.ui.chip.port
arrayfactor.ui.chip.step
arrayfactor.core.utils ;
IN: arrayfactor.sessions.okad.read

: read-test-block ( block -- seq )
    "sessions/okad/OkadWork.cf" block-file-contents swap read-block ;

! 842 read-test-block
! "sessions/okad/OkadWork.cf" block-file-contents 842 extract-block
! "sessions/okad/OkadWork-parpi1.cf" block-file-contents 410 read-block
! "sessions/okad/OkadWork-parpi1.cf" block-file-contents 410 extract-block

#! USE: arrayfactor.sessions.okad.read
