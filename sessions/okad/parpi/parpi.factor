USING: kernel io io.directories io.pathnames
arrayfactor.chip
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.smart-integer
arrayfactor.core.preferences
arrayfactor.projects.parpi
arrayfactor.ui
arrayfactor.chip.node.f18a.compilation.cfs.blocks
arrayfactor.ui.chip.node.f18a.memory
arrayfactor.ui.chip.nodes.navigation
arrayfactor.ui.chip.nodes
arrayfactor.ui.chip.port
arrayfactor.ui.chip.step ;
IN: arrayfactor.sessions.okad.parpi

parpi

fdir "tests/OkadWork/OkadWork.cf"
append-path block-file-contents

720 write-blocks

fdir "C:/KS/factor/arrayfactor/sessions/okad/parpi/OkadWork.cf"
append-path write-block-file

"Blocks written from parpi to OkadWork.cf\n" write

#! USE: arrayfactor.sessions.okad.parpi
