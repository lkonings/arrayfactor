#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: kernel math namespaces sequences ;
IN: arrayfactor.chips

SYMBOL: Chip#
CONSTANT: Chips V{ }

: last-chip-active ( -- )
    Chips length 1 -
    Chip# set-global ;

: (no-chips) ( -- )
    Chips dup length 0 >
    [ pop drop (no-chips) ] [ drop ] if
; recursive

: no-chips ( -- )
    (no-chips) last-chip-active ;

: remove-chip ( -- )
    Chips dup length 0 >
    [ pop drop last-chip-active ] [ drop ] if ;

: this-chip ( -- chip )
    Chip# get-global Chips nth ;

ERROR: invalid-chip-error ;

: >current-chip ( n -- )
    dup Chips ?nth
    [ Chip# set-global ]
    [ drop invalid-chip-error ]
    if ;
