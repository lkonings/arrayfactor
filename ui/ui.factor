! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.node
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.memory
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.port
arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.core.preferences
arrayfactor.core.utils
arrays assocs calendar colors
colors.constants combinators fonts grouping hashtables io
io.styles kernel locals math math.parser math.ranges models
models.arrow namespaces sequences splitting strings timers ui
ui.gadgets ui.gadgets.borders ui.gadgets.grid-lines
ui.gadgets.grids ui.gadgets.labels ui.gadgets.packs
ui.gadgets.panes ui.gadgets.scrollers ui.pens ui.pens.solid
unicode.normalize vectors ;
IN: arrayfactor.ui

SYMBOL: Font-Size

: get-font-size ( -- n )
    Font-Size get-global ;

: set-font-size ( n -- )
    Font-Size set-global ;

TUPLE: ui
    instructions-gadget
    navigation-gadget
    node-gadget
    return-stack-gadget
    registers-gadget
    stack-gadget ;

: <ui> ( -- ui )
    f                                 #! instructions-gadget
    f                                 #! navigation-gadget
    f                                 #! node-gadget
    f                                 #! registers-gadget
    f                                 #! return-stack-gadget
    f                                 #! stack-gadget
    ui boa ;

: init-ui ( -- )
    <ui> this-chip ui<<
    12 set-font-size ;

CONSTANT: height 10

: normal-border ( gadget -- bordered-gadget )
    height dup 2array <border> ; inline

: solid-gray-boundary ( gadget -- gadget' )
    COLOR: gray <solid> >>boundary ; inline

: string>label ( s -- label )
    <label> dup prefer normal-border ;

: >label-control ( model -- bordered-label )
    <label-control> dup prefer normal-border ;

: number>label ( n -- label )
    number>string string>label ;

: empty-label ( -- label )
    "" string>label 1vector ;

: monospace-font>label ( bordered-label -- )
    children>> first monospace-font >>font drop ;

: serif-font>label ( bordered-label -- )
    children>> first serif-font >>font drop ;

: foreground-color>label ( rgba bordered-label -- )
    children>> first font>> foreground<< ;

: red ( -- rgba )
    1.0 0.0 0.0 0.0 <rgba> ;

: black ( -- rgba )
    0.0 0.0 0.0 0.0 <rgba> ;

:: return-addr>string ( h addr ram|rom -- string )
    h addr dup
    ram|rom get-addr-list dup empty?
    [ 3drop ]
    [ nth dup ram|rom addr>name drop
      " -" prepend "+#" append
      [ - number>string ] dip prepend
      append
    ] if ;

: smart-integer-named ( smart-integer n -- string )
    swap
    [ name> ] [ sequence#> number>string ] bi
    "-" prepend append " " prepend append ;

:: smart-integer-unnamed ( smart-integer h node -- string )
    smart-integer a-return-addr?
    [ h smart-integer
      n> node memory>> (ram|rom)
      return-addr>string
    ] [ h ] if ;

:: smart-integer&node>string ( smart-integer node -- string )
    smart-integer dup [ base>string ] [ named? ] bi
    [ smart-integer-named ] [ node smart-integer-unnamed ] if ;

: arrow-control ( model node index -- label-control )
    [ rot* nth swap smart-integer&node>string ] 2curry
    <arrow> >label-control
    solid-gray-boundary ;

: width>label-control ( model width -- bordered-label )
    [ <label-control> dup prefer ] dip
    height 2array <border> ;

CONSTANT: width 30

: integer>hex-arrow ( code-model -- label-control )
    [ >hex ] <arrow> width width>label-control ;

: smart-integer>hex-arrow ( code-model -- label-control )
    [ n> >hex ] <arrow> width width>label-control ;

: pane-styles ( smart-integer -- hash )
    H{ } clone get-font-size font-size rot* ?set-at
    swap color-name/f>>
    [ named-color foreground rot* ?set-at ] when* ;

: smart-integer>pane ( smart-integer -- )
    [ smart-integer>string ]
    [ pane-styles ]
    bi format ;

: normal-style ( -- hash )
    H{ } clone
    get-font-size font-size rot* ?set-at
    "black" named-color foreground rot* ?set-at ;

: normal-string>pane ( string -- )
    normal-style format ;

: <normal-pane-control> ( model quot -- border )
    f pane-control new-pane
    swap >>quot swap >>model
    normal-border ;

#! -----------------

: window-title ( title node# -- string )
    [ number>string append ]
    [ node#>node get-node-name append-string ] bi ;
