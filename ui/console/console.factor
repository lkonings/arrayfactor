! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrayfactor.chip
arrayfactor.chip.node arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation
arrayfactor.chip.node.f18a.compilation.cfs.blocks
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.stacks arrayfactor.chip.nodes
arrayfactor.chips arrayfactor.core.utils arrayfactor.ui fry
help.vocabs kernel math sequences threads
vocabs vocabs.loader ;
IN: arrayfactor.ui.console

<PRIVATE

: add-expanded-to-source ( source-string -- )
    this-compilation
    [ swap expand-source ]
    [ expanded>> ]
    [ swap [ append ] curry change-source drop ] tri ;

ERROR: no-current-node-error ;

: check-for-current-node ( -- )
    this-node [ no-current-node-error ] unless ;

ERROR: not-an-integer-error ;

: check-for-integer ( n -- n )
    !TODO Check for 18-bits number
    dup integer? [ not-an-integer-error ] unless ;

: assemble ( -- )
    check-for-current-node
    this-compilation get-source
    this-assembly [ assemble-source ] [ end-assembly ] bi ;

! : into-word ( addr -- )
!    this-chip step>> 0 <
!    [ this-assembly replace-a9-code ]
!    [ this-node [ p< ] [ p>instruction-stack ] bi
!    ] if ;

: new-assembly ( node -- )
    this-ram init-ram
    this-assembly memory>> <assembly> >>assembly drop ;

ERROR: word-not-found-error ;

PRIVATE>

: {node ( node# -- )
    dup node#>nd this-chip get-node
    [ >current-node ] [ {node} ] if ;

: node} ( -- )
    check-for-current-node
    this-compilation
    dup source>> [ " " join ] [ empty? ] bi
    [ this-node new-assembly ] unless
    compile-source assemble ;

ALIAS: && node}

: &&about ( -- )
    "arrayfactor" about ;

: &&about-console ( -- )
    "arrayfactor.ui.console" about ;

: &&chip ( -- )
    <ga144> <chip> drop
    init-ui start-stepping ;

: &&current-chip ( n -- )
    >current-chip ;

: &&name>node# ( name -- node#/f )
    this-chip nodes>> sift
    swap '[ name>> _ = ] filter
    dup empty? [ drop f ]
    [ first node#>> ]
    if ;

! : &&into ( word-name -- )
!    check-for-current-node
!    this-ram name>addr
!    [ into-word ]
!    [ drop word-not-found-error ]
!    if ;

: &&halt ( -- )
    t this-chip halt<< ;

: &&steps ( n -- )
    check-for-current-node
!   [ steps ] curry in-thread ; inline
    steps ; inline

: &&step ( -- )
    check-for-current-node
    1 steps ;

: &&step# ( -- n )
    check-for-current-node
    this-chip step>> ;

: &&stopped? ( -- ? )
    check-for-current-node
    this-node stacks>> stopped>> ;

: &&clear ( -- )
    check-for-current-node
    this-compilation clear-source ;

: &&source ( -- source-string )
    check-for-current-node
    this-compilation source>> " " join format-source ;

: &&debug-info ( -- hash )
    this-chip debug-info>> hash>> ;

: &&until=addr@ ( addr n max-steps -- )
    check-for-current-node
    [ this-node node#>> ] 3dip step-until-addr-contains ;

: &&until-active ( max-steps -- )
    check-for-current-node
    this-node node#>> swap step-until-active ;

: &&until-stopped ( max-steps -- )
    check-for-current-node
    this-node node#>> swap step-until-stopped ;

: &&until=p ( n max-steps -- )
    check-for-current-node
    [ this-node node#>> ] 2dip step-until-p ;

: &&until=t ( n max-steps -- )
    check-for-current-node
    [ this-node node#>> ] 2dip step-until-t ;

! : &&until-port-active ( node1# node2# max-steps -- )
!    check-for-current-node
!    step-until-port-active ;

#!
#! GREENARRAYS BLOCK FILES
#!

: &&block-file-contents ( path -- file-byte-array )
    check-for-current-node
    block-file-contents ;

: &&write-file-contents ( file-byte-array path -- )
    check-for-current-node
    write-block-file-contents ;

: &&this-node>block ( block file-byte-array -- file-byte-array' )
    check-for-current-node
    this-node node>packed-vector
    rot* write-block ;

: &&block>array ( file-byte-array block -- seq )
    read-block ;

#!
#! BASE
#!

: &&stack-base ( n -- )
    check-for-current-node
    this-node stacks>> change-stack-bases ;

: &&return-stack-base ( n -- )
    check-for-current-node
    this-node stacks>> change-return-stack-bases ;

"arrayfactor.ui.console.access" require
"arrayfactor.ui.console.instructions" require
"arrayfactor.ui.console.windows" require

! USE: arrayfactor.ui.console
