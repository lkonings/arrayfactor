! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip arrayfactor.chip.node
arrayfactor.ui.chip.node arrayfactor.ui.chip.node.node-editor
arrayfactor.chip.node.f18a
arrayfactor.ui.chip.node.f18a.instructions
arrayfactor.ui.chip.node.f18a.memory.ram
arrayfactor.ui.chip.node.f18a.memory.rom
arrayfactor.ui.chip.node.f18a.registers
arrayfactor.ui.chip.node.f18a.stacks
arrayfactor.ui.chip.nodes arrayfactor.ui.chip.nodes.navigation
arrayfactor.ui.chip.port arrayfactor.ui.console.private ;
IN: arrayfactor.ui.console.windows

: &&navigation ( -- )
    check-for-current-node
    this-node node#>>
    navigation-window
    >current-node ;

: &&editor ( -- )
    check-for-current-node
    this-node node#>> editor-window ;

: &&instructions ( -- )
    check-for-current-node
    this-node node#>> instructions-window ;

: &&port ( node#1 node#2 -- )
    check-for-current-node
    port-window ;

: &&node ( -- )
    check-for-current-node
    this-node node#>> node-window ;

: &&nodes ( -- )
    check-for-current-node
    nodes-window
    navigation-window ;

: &&4nodes ( -- )
    4-nodes-window
    navigation-window ;

: &&40nodes ( -- )
    40-nodes-window
    navigation-window ;

: &&ram ( -- )
    check-for-current-node
    this-node node#>> ram-window ;

: &&registers ( -- )
    check-for-current-node
    this-node node#>> registers-window ;

: &&rom ( -- )
    check-for-current-node
    this-node node#>> rom-window ;

: &&stack ( -- )
    check-for-current-node
    this-node node#>> stack-window ;

: &&return-stack ( -- )
    check-for-current-node
    this-node node#>> return-stack-window ;
