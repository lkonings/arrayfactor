! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: calendar help.markup help.syntax math prettyprint
quotations sequences strings ;
IN: arrayfactor.ui.console.windows

HELP: &&40nodes
{ $description "Open 40 nodes window." } ;

HELP: &&4nodes
{ $description "Open 4 nodes window." } ;

HELP: &&editor
{ $description "Open editor window." } ;

HELP: &&instructions
{ $description "Open instructions window." } ;

HELP: &&navigation
{ $description "Open navigation window." } ;

HELP: &&node
{ $description "Open node window." } ;

HELP: &&nodes
{ $description "Open nodes window." } ;

HELP: &&port
{ $values { "node#1" integer "node#2" integer } }
{ $description "Open port window of port between the two nodes." } ;

HELP: &&ram
{ $description "Open ram window." } ;

HELP: &&registers
{ $description "Open registers window." } ;

HELP: &&return-stack
{ $description "Open return-stack window." } ;

HELP: &&rom
{ $description "Open rom window." } ;

HELP: &&stack
{ $description "Open stack window." } ;

ARTICLE: "arrayfactor.ui.console.windows" "Console windows"
{ $link "arrayfactor.ui.console" } $nl
"The " { $vocab-link "arrayfactor.ui.console.windows" } " opens certain windows."
{ $heading "Node(s) windows" }
{ $subsections &&40nodes &&4nodes &&node &&nodes }
{ $heading "Memory windows" }
{ $subsections &&ram &&rom }
{ $heading "Port window" }
{ $subsections &&port }
{ $heading "Registers window" }
{ $subsections &&registers }
{ $heading "Stack windows" }
{ $subsections &&return-stack &&stack }
;

ABOUT: "arrayfactor.ui.console.windows"
