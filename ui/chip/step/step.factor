! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip.nodes arrayfactor.chips
arrayfactor.ui.chip.port calendar kernel timers ;
IN: arrayfactor.ui.chip.step

: stepping ( seconds-interval steps# -- timer )
    [ steps ] curry swap seconds every ;

! USE: arrayfactor.ui.chip.step
