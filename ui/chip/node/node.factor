! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays
colors.constants combinators generalizations grouping
hashtables io io.styles kernel locals math math.order
math.parser models models.product namespaces sequences strings
ui ui.gadgets.grids ui.gadgets.labels
ui.gadgets.panes vectors
arrayfactor.chip
arrayfactor.chip.node arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.node.f18a.status
arrayfactor.chip.port arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.core.utils
arrayfactor.ui ;
FROM: arrayfactor.core.utils => ?first ;
IN: arrayfactor.ui.chip.node

TUPLE: node-pane-control < pane-control node# ;

TUPLE: standard-pane-control < pane-control node# ;

TUPLE: node-gadget < grid ;

TUPLE: node-info
    { view-stack-items integer }
    { view-return-stack-items integer } ;

: <node-info> ( stack-items-seq -- node-info )
    [ first 10 min ]     #! view-stack-items
    [ second 9 min ] bi  #! view-return-stack-items
    node-info boa  ;

#!
#! OPCODE TO STRING
#!

: v>string ( v -- s )
    first2 [ >hex ] dip 2array " " join ;

: vi>string ( v|i -- s )
    dup integer? [ >hex ]
    [ dup integer-name? [ drop "" ] [ v>string ] if ] if ;

: vis>string ( v|i|s -- string )
    dup string? [ vi>string ] unless ;

: visf>string ( v|i|s|f -- string )
    dup [ vis>string ] [ drop "f" ] if ;

: f>surround ( 2vector -- 1vector )
    dup first "f" = [ second "(" ")" surround ] [ second ] if ;

: f's>surround ( vector -- vector' )
    clone dup "" swap push reverse 2 clump
    [ second "f" = not ] filter
    [ f>surround ] map ;

: surrounded-operation ( vectors -- string )
    concat dup length 0 >
    [ [ visf>string ] map f's>surround
      ?first dup [ drop "" ] unless ]
    [ drop "" ] if ;

: blocking>string ( status -- string )
    blocking-> [ " *" ] [ "" ] if ;

: ((not-)blocking-count>string) ( status -- string )
    (not-)blocking-count-> number>string " - " string-prepend?! ;

: operation>string ( node vectors -- string )
    surrounded-operation
    swap status>>
    [ blocking>string ] [ ((not-)blocking-count>string) ] bi
    append append ;

: attach-to-register ( node smart-integer -- string )
    n> dup port#?
    [ [ nd>> ] dip port#>nsew ] [ 2drop "" ] if ;

: (opcode>string) ( node -- string )
    opcode-> ?first dup [ visf>string ] [ drop "" ] if ; inline

: (node-direction>string) ( node -- string )
    [ (opcode>string) { "@b" "!b" } member? ]
    [ swap [ b> ] [ a> ] if ]
    [ swap attach-to-register ] tri ;

: node-direction>string ( node -- string )
    dup status>> blocking->
    [ (node-direction>string) ] [ drop "" ] if ;

: node-header-styles ( color -- hash )
    H{ } clone swap
    named-color foreground rot* ?set-at
    get-font-size font-size rot* ?set-at ;

: <pane-border> ( node# model quot -- border )
    f 4 npick is-real-node#?
    [ node-pane-control ] [ standard-pane-control ] if
    new-pane
    swap >>quot swap >>model swap >>node#
    normal-border solid-gray-boundary ;

: standard-node?! ( node -- node/standard-node ? )
    dup is-real-node?
    [ [ drop Standard-Node get-global ] unless ]
    keep ;

: append-direction ( string node blocking? -- string' )
    [ node-direction>string append-string ] [ drop ] if ;

: node-style-for-stopped? ( node -- color-name/f ? )
    stacks>> stopped>>
    [ [ "magenta" ] [ f ] if ] keep ;

: node-styles ( node blocking? -- styles )
    over node-style-for-stopped?
    [ 2nip ]
    [ drop
      [ status>> writing>>
        [ "DarkRed" ] [ "DarkOrange" ] if ]
      [ is-real-node?
        [ "DarkGreen" ] [ "black" ] if ]
      if
    ] if
    node-header-styles ;

:: node>pane ( node# node -- )
    node# number>string
    " " node get-node-name append append
    node dup status>> blocking->
    [ append-direction ]
    [ node-styles ] 2bi format ;

: node#>pane ( node# -- )
    number>string "black" node-header-styles format ;

:: (node-pane) ( node# node real-node? -- pane-border )
    node# node status>> blocking-model-> 
    node# node real-node?
    [ [ node>pane drop ] 2curry ]
    [ [ drop node#>pane drop ] 2curry ]
    if <pane-border> ;

: {node-pane} ( node# node -- pane-border )
    standard-node?! (node-pane) ;

: node-pane ( node# -- pane-border )
    dup node#>nd this-chip get-node {node-pane} ;

: smart-integer&node>pane ( smart-integer node -- )
    [ smart-integer&node>string ]
    [ drop pane-styles ]
    2bi format ;

: (init-registers-product) ( node -- product )
    { [ a-model> ] [ b-model> ] [ p-model> ]
      [ instruction-stack-model-> ] [ opcode-model-> ]
    } cleave [ 3array ] 2dip 2vector append <product> ; inline

: register-format ( node smart-integer -- )
    [ swap smart-integer&node>pane ]
    [ attach-to-register " - " prepend normal-string>pane nl ]
    2bi ;

: (the-registers-format) ( node vectors -- )
    [ "a: " normal-string>pane first register-format ]
    [ "b: " normal-string>pane second register-format ]
    [ "p: " normal-string>pane
      nip third >hex "$" prepend normal-string>pane nl ]
    2tri ;

: the-registers-format ( node vectors -- )
    [ 3 head (the-registers-format) ]
    [ 3 tail operation>string normal-string>pane ]
    2bi ;

: {registers-pane} ( node# node -- pane-border )
    standard-node?! drop
    [ (init-registers-product) ]
    [ [ swap the-registers-format ] curry ] bi
    <pane-border> ;

: registers-pane ( node# -- pane-border )
    dup node#>node {registers-pane} ;

: stack>pane ( items node vector -- )
    reverse rot* head swap
    [ smart-integer&node>pane nl ] curry each ;

: {stack-pane} ( node# node-info node -- pane-border )
    standard-node?! drop
    [ view-stack-items>> ] dip
    [ nip stacks>> stack-model-> ]
    [ [ rot* stack>pane ] 2curry ] 2bi <pane-border> ;

: stack-pane ( node-info node# -- pane-border )
    tuck* node#>node {stack-pane} ;

: {return-stack-pane} ( node# node-info node -- pane-border )
    standard-node?! drop
    [ view-return-stack-items>> ] dip
    [ nip stacks>> return-stack-model-> ]
    [ [ rot* stack>pane
    ] 2curry ] 2bi <pane-border> ;

: return-stack-pane ( node-info node# -- pane-border )
    tuck* node#>node {return-stack-pane} ;

: (node-gadget) ( node-info node# -- array )
    { [ nip node-pane ]
      [ nip registers-pane ]
      [ stack-pane ]
      [ return-stack-pane ]
    } 2cleave
    4array [ 1array ] map ;

: <node-gadget-vertical> ( node-info node# -- gadget )
    (node-gadget)
    node-gadget new-grid ;

: <node-gadget> ( node-info node# -- gadget )
    (node-gadget) flip
    node-gadget new-grid
    { 300 170 } >>pref-dim ;

: new-node-gadget ( node# -- nodegadget )
    { 10 9 } <node-info>
    swap <node-gadget-vertical>
    { 200 450 } >>pref-dim
    dup this-chip ui>> node-gadget<< ;

: (node-window) ( node -- )
    dup new-node-gadget
    "Node: " rot* window-title
    open-window ;

: node-window ( node# -- )
    dup is-real-node#? 
    [ dup node#>nd this-chip get-node
      [ dup {node} ] unless
      (node-window)
    ] [ drop ] if ;

: current-node-window ( -- )
    this-chip this-node-model->
    [ notify-connections ]
    [ [ node#>> new-node-gadget gadget. ] <pane-control> ] bi
    "Current node" open-window ;

! USE: arrayfactor.ui.chip.node
