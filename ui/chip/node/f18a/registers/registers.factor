! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrays kernel
math.parser models.arrow sequences
ui ui.gadgets.borders ui.gadgets.grids
arrayfactor.chip
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.ui ;
IN: arrayfactor.ui.chip.node.f18a.registers

TUPLE: registers-gadget < grid ;

CONSTANT: width 30

: header-row ( -- array )
    "a" "b" "p"
    [ string>label normal-border solid-gray-boundary ]
    tri@ 3array ;

: registers-row ( node -- array )
    [ a-model> smart-integer>hex-arrow ]
    [ b-model> smart-integer>hex-arrow ]
    [ p-model> integer>hex-arrow ] tri
    [ normal-border solid-gray-boundary ] tri@
    3array ;

: registers-array ( node -- array )
    header-row swap registers-row 2array ;

: <registers-gadget> ( node# -- node registers-gadget )
    node#>nd this-chip get-node dup
    registers-array <grid>
    { 2 2 } >>gap { 15 5 } <border> ;

: registers-title ( node -- string )
    "Registers - Node: " swap
    [ node#>> number>string append " " append ]
    [ get-node-name append ] bi ;

: registers-window ( node# -- )
    dup is-real-node#? 
    [ <registers-gadget>
      dup this-chip ui>> registers-gadget<<
      swap registers-title open-window
    ] [ drop ] if ;

! USE: arrayfactor.ui.chip.node.f18a.registers
