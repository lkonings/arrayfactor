! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays assocs colors
colors.constants combinators fry grouping
io kernel locals math math.parser math.ranges models
models.arrow models.product models.range
namespaces sequences splitting ui ui.baseline-alignment
ui.gadgets ui.gadgets.borders ui.gadgets.buttons
ui.gadgets.editors ui.gadgets.grids ui.gadgets.grid-lines
ui.gadgets.labels ui.gadgets.packs ui.gadgets.scrollers
ui.gadgets.sliders ui.gadgets.tracks ui.pens ui.pens.solid
arrayfactor.chip
arrayfactor.chip.node
arrayfactor.chip.nodes
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.execution
arrayfactor.chips
arrayfactor.core.preferences
arrayfactor.ui
arrayfactor.ui.chip.nodes ;
FROM: models.range => <range> ;
IN: arrayfactor.ui.chip.nodes.navigation

TUPLE: navigation-gadget < track tf step-model ;

M: navigation-gadget focusable-child* tf>> ;

: get-tf-number ( navigation -- n )
    tf>> dup editor-string string>number dup
    [ nip ]
    [ drop "1" swap set-editor-string 1 ]
    if ;

: steps: ( f -- )
    drop
    this-chip ui>> navigation-gadget>>
    [ get-tf-number steps ]
    [ this-chip step>> number>string swap
      step-model>> set-model ] bi ;

: step-button ( -- button )
    f f \ steps: <command-button> { 0 0 } <border> ;

: step-tf ( navigation -- navigation )
    <editor> >>tf ;

: step# ( navigation -- navigation label )
    this-chip step>> number>string <model>
    [ over step-model<< ] [ >label-control ] bi ;

: <tf> ( navigation -- gadget )
    tf>>
    "1" over set-editor-string
    "" label-on-left { 25 5 } <border>
    COLOR: gray <solid> >>boundary ;

: button&tf ( navigation -- track )
    horizontal <track>
      0 >>fill
      +baseline+ >>align
      { 10 5 } >>gap
    step-button f track-add
    swap <tf> f track-add ;

: <navigation-gadget> ( -- gadget )
    vertical navigation-gadget new-track
    step-tf
    dup button&tf f track-add
    step# f track-add
    dup tf>> request-focus ;

: navigation-window ( -- )
    0 >current-node
    <navigation-gadget>
    dup this-chip ui>> navigation-gadget<<
        { 75 50 } <border> { 1 1 } >>fill
    "Navigation" open-window ;

! USE: arrayfactor.ui.chip.nodes.navigation
