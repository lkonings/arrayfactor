! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrays grouping math tools.test namespaces make
sequences kernel quotations
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.connection
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation.cfs
arrayfactor.chip.node.f18a.compilation
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chips
arrayfactor.core.library.macros.basic
arrayfactor.tests.parpi
arrayfactor.chip.smart-integer ;
FROM: arrayfactor.chip.node.f18a.execution => # ;
IN: arrayfactor.tests.parpi.tests

: n#  ( n -- ) <smart-integer> this-node # ;
: n#> ( -- n ) this-node #> n> ;

<ga144> <chip> drop

#! CONTROL - NODE 0

[ V{
    "#0 [org .br"
    ":eternal ]eternal ];"
    ":main   ]#10000 (nm>t (limit ]dup"
    "[cnn ]lit ]b! ]!b .cr"
    "[cnn1 ]lit ]b! ]!b .br"
    "(main ]; .br"
    ":init ]main ]eternal ]; .br"
    "$A9 [org ]init ]; .cr" }
V{
    "0" "org"
    ":" "eternal" "eternal" ";"
    ":" "main" "10000" "dup" "[" "cnn" "]" "lit"
        "b!" "!b" "[" "cnn1" "]" "lit" "b!" "!b" ";"
    ":" "init" "main" "eternal" ";"
    "[" "169" "org" "]" "init" ";"
} H{ { "eternal" 0 } { "init" 7 } { "main" 1 } }
V{
    V{ f V{ 0 "jmp" } }
    V{ f "@p" "dup" f "@p" "." }
    V{ 10000 }
    V{ 373 }
    V{ "b!" f "!b" f "@p" "." }
    V{ 373 }
    V{ "b!" f "!b" f ";" }
    V{ f V{ 1 "call" } }
    V{ f V{ 0 "jmp" } }
} ]
[ "projects/parpi/hex-stuff/control.txt" "projects/parpi/cfs/218.cfs" 0 ld
  this-compilation get-cfs get-cfs-source
  this-compilation get-cfs get-cfs-new-source
  this-compilation dup get-cfs get-cfs-new-source " " join compile-source
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  this-ram get-names
  this-ram get-timed-code*
  "init" this-ram name>addr drop this-assembly replace-a9-code 
] unit-test

#! GENERATOR - NODE 1

[ V{
    "(parpi (generat +or #0"
    "[org .cr"
    ":aap (lc-lc ]ne, ]if"
    "]drop ]dup ]!b ]#4"
    "]. ]+ ]; .cr"
    "]then ]drop ];"
    ":main   (hl- ]begin ]ne, ]while ]drop ]dup ]!b ]#2 ]. ]+ .cr"
    "]aap ]end ]drop ]drop ];"
    ":main1  (lv-"
    "]begin ]ge, .cr"
    "]-whil +e ]drop ]dup ]!b ]#6 ]."
    "]+ ]end ]drop ]drop ];"
    ":init .indent"
    "[cnp ]lit ]a!"
    "[cnn ]lit ]b!"
    "]@"
    "dup ]!b"
    "]#2 ]!b"
    "]#23 ]#5 ]main"
    "]main1"
    "dup or ]!b .cr"
    "]init ]; .br"
    "$A9 [org ]init ];"
} ]
[ "tests/parpi/hex-stuff/generator.txt" "tests/parpi/cfs/220.cfs"  1 ld
  this-compilation get-cfs get-cfs-source
! this-compilation get-cfs get-cfs-new-source
! this-compilation get-cfs "tests/parpi/cfs/220.factor" write-new-source
] unit-test

#! MERGE - NODE 8

[ V{
    "(merge #0 [org .br"
    ":le,, ]- ]+ ;"
    ":0@ [cnp ]lit ]a! ]@ ];"
    ":1@ [cnp1 ]lit ]a! ]@ ];"
    ":main  ]over ]over ]le,, ]-if .cr"
    "]drop ]over ]!b"
    "]0@ ]if ]over ]main ]; ]then .cr"
    "]drop [cnp1 ]lit ]a! ]; ]then .cr"
    "]drop ]!b"
    "]. ]1@ ]if ]main ]; ]then .cr"
    "]drop [cnp ]lit ]a! ]; .br"
    ":finish   ]!b ]begin ]@ ]while ]!b ]end ]!b"
    "];"
    ":init    [cnn ]lit ]b!"
    "]. ]0@ ]1@ ]!b ]0@ ]1@ ]!b"
    "]0@ ]1@ ]main ]finish ]; .br"
    "$A9 [org ]init ];"
} V{
    "0" "org"
    ":" "le,," "-" "+" ";"
    ":" "0@" "[" "cnp" "]" "lit" "a!" "@" ";"
    ":" "1@" "[" "cnp1" "]" "lit" "a!" "@" ";"
    ":" "main" "over" "over" "le,," "-if" "drop" "over"
        "!b" "0@" "if" "over" "main" ";"
        "then" "drop" "[" "cnp1" "]" "lit" "a!" ";"
        "then" "drop" "!b" "." "1@" "if" "main" ";"
        "then" "drop" "[" "cnp" "]" "lit" "a!" ";"
    ":" "finish" "!b" "begin" "@" "while" "!b" "end" "!b" ";"
    ":" "init" "[" "cnn" "]" "lit" "b!" "." "0@" "1@" "!b"
        "0@" "1@" "!b" "0@" "1@" "main" "finish" ";"
    "[" "169" "org" "]" "init" ";"
} ]
[ "tests/parpi/hex-stuff/merge.txt" "tests/parpi/cfs/228.cfs" 8 ld
  this-compilation get-cfs get-cfs-source
  this-compilation get-cfs get-cfs-new-source
! this-compilation get-cfs "tests/parpi/cfs/228.factor" write-new-source
] unit-test

#! RESULT - NODE 700

[ V{
    "(parpi (result +s #0"
    "[org .br"
    ":main ,1D ]@b ]@b (main ]; .br"
    ":init ,3D [cnp ]lit ]b! ]main ]; .br"
    "$A9 [org ]init ];"
} V{
    "0" "org" ":" "main" "@b" "@b" ";"
    ":" "init" "[" "cnp" "]" "lit" "b!" "main" ";"
    "[" "169" "org" "]" "init" ";"
} ]
[ "tests/parpi/hex-stuff/result.txt" "tests/parpi/cfs/226.cfs" 700 ld
  this-compilation get-cfs get-cfs-source
  this-compilation get-cfs get-cfs-new-source
! this-compilation get-cfs "tests/parpi/cfs/226.factor" write-new-source
] unit-test

#! SIEVE1 - MANY NODES

[ V{
    "(parpi (early (sieve #0"
    "[org"
    ":-swap   ]- ]. ]+ ]- ];"
    ":gcnt   ]begin ]@ ]while ]drop ]#1 ]. ]+ ]end ]drop ];"
    ":sv   ]ne, ]if ]drop ]!b ]; ]then ]drop ]drop ];"
    ":sieve   ]begin ]@ ]while .cr"
    "]begin ]lt, ]-whil +e ]drop ]push ]over ]. ]+ ]pop ]end ]drop"
    "]sv"
    "]end ]; .br"
    ":svcnt   ]push ]-if ]drop ]over ]#1 ]. ]+ ]!b ]pop ]sieve ]; ]then"
    "]pop ]drop ]drop ]drop ]#1 ]. ]+ ]gcnt ]!b ];"
    ":?actn ]push ]if ]pop"
    "]over ]dup ]+ ]dup"
    "]push ]-swap ]pop ]svcnt"
    "]; ]then ]pop ]drop"
    "]over ]!b ];"
    ":main   ]@ ]dup ]!b ]push ]@ ]@ ]pop"
    "]?actn   ]dup ]or ]!b   ]main ];"
    ":init [cnp ]lit ]a!"
    "[cnn ]lit ]b! ]main"
    "]init ]; .br"
    "$A9 [org ]init ];"
} ]
[ "tests/parpi/hex-stuff/early-sieve.txt" "tests/parpi/cfs/222.cfs" 300 ld
  this-compilation get-cfs get-cfs-source
! this-compilation get-cfs get-cfs-new-source
! this-compilation get-cfs "tests/parpi/cfs/222.factor" write-new-source
] unit-test

#! SIEVE2 - MANY NODES

[ V{
    "(parpi (late (sieve #0"
    "[org"
    ":-swap   ]- ]. ]+ ]- ];"
    ":*,   ]a ]push ]dup ]a! ]dup ]or"
    "]#17 ]for ]. ]+* ]unext"
    "]drop ]drop ]a ]pop ]a! ];"
    ":gcnt   ]begin ]@ ]while ]drop ]#1 ]. ]+ ]end ]drop ];"
    ":sv   ]ne, ]if ]drop ]!b ]; ]then ]drop ]drop ];"
    ":sieve   ]begin ]@ ]while .cr"
    "]begin ]lt, ]-whil +e ]drop ]push ]over ]. ]+ ]pop ]end ]drop"
    "]sv"
    "]end ]; .br"
    ":svcnt   ]push ]-if ]drop ]over ]#1 ]. ]+ ]!b ]pop ]sieve ]; ]then"
    "]pop ]drop ]drop ]drop ]#1 ]. ]+ ]gcnt ]!b ];"
    ":?actn   ]push ]if ]pop ]over ]dup ]*, ]dup ]push ]-swap ]pop"
    "]svcnt ]; ]then"
    "]pop ]drop ]over ]!b ];"
    ":main   ]@ ]dup ]!b ]push ]@ ]@ ]pop"
    "]?actn   ]dup ]or ]!b   ]main ];"
    ":init   [cnp ]lit ]a!   [cnn ]lit ]b!   ]main   ]init ]; .br"
    "$A9 [org ]init ];"
} ]
[ "tests/parpi/hex-stuff/late-sieve.txt" "tests/parpi/cfs/224.cfs" 400 ld
  this-compilation get-cfs get-cfs-source
! this-compilation get-cfs get-cfs-new-source
! this-compilation get-cfs "tests/parpi/cfs/224.factor" write-new-source
] unit-test

[ { 54 56 } { 55 57 } ]
[ define-macros
  300 {node}
  "init" "tests/parpi/hex-stuff/late-sieve.txt" "tests/parpi/cfs/224.cfs" 301 303 lds
  301 >current-node
  this-compilation connection>> [ get-pnd ] [ get-nnd ] bi 2array
  302 >current-node
  this-compilation connection>> [ get-pnd ] [ get-nnd ] bi 2array
] unit-test

[ { 72 74 } { 75 111 } ]
[ 400 {node}
  "init" "tests/parpi/hex-stuff/late-sieve.txt" "tests/parpi/cfs/224.cfs" { 401 403 603 } ldss
  401 >current-node
  this-compilation connection>> [ get-pnd ] [ get-nnd ] bi 2array
  503 >current-node
  this-compilation connection>> [ get-pnd ] [ get-nnd ] bi 2array
] unit-test

#! NODES

: node-num ( node -- n/f ) dup [ node#>> ] when ;

[ {
    {   0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17 }
    { 100 101 102 103 104 105 106 107 108   f 110 111 112 113 114 115 116 117 }
    {   f   f   f   f   f   f   f   f   f   f 210 211 212 213 214 215 216 217 }
    { 300 301 302 303 304 305 306 307 308 309 310 311 312 313 314 315 316 317 }
    { 400 401 402 403 404 405 406 407 408 409 410 411 412 413 414 415 416 417 }
    { 500 501 502 503 504 505 506 507 508 509 510 511 512 513 514 515 516 517 }
    { 600 601 602 603 604 605 606 607 608 609 610 611 612 613 614 615 616 617 }
    { 700 701 702 703 704 705 706 707 708 709 710 711 712 713 714 715 716 717 }
} ]
[ parpi
  this-chip nodes>>
  [ node-num ] map 18 group
] unit-test

remove-chip

#! "arrayfactor.tests.parpi" test
