! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: arrayfactor.chip arrayfactor.chip.node.f18a.execution
arrayfactor.chip.nodes arrayfactor.chip.smart-integer
arrayfactor.chips arrayfactor.core.utils
arrayfactor.tests.parpi arrayfactor.ui
arrayfactor.ui.chip.nodes arrayfactor.ui.chip.nodes.navigation
arrayfactor.ui.chip.step arrayfactor.ui.console
arrayfactor.ui.console.access io kernel ;
IN: arrayfactor.tests.parpi.test

: &result ( limit -- )
    700 swap step-until-active ;

ERROR: parpi-test-failed-error ;

: test-result ( -- )
    700 {node
    /t@ HEX: 4cd =
    [ "parpi-test successful!\n" write ]
    [ parpi-test-failed-error ]
    if ;

: parpi-test ( keep-chip? -- )
    parpi
    hardcoded-ram
    init-ui

    start-stepping
    100 steps
    123456 &result
    100 steps
    123456 &result
    
    test-result
    
    [ remove-chip ] unless ;
    
! &nodes

! Change limit in 218.cfs!

! Primes under 1,000 = 0x3e8
! Result: 1229 = 0xa8

! Primes under 10,000 = 0x2710
! Result: 1229 = 0x4cd

! Primes under 100,000 = 0x186a0
! Result: 9592 = 0x2578

! USE: arrayfactor.tests.parpi.test
