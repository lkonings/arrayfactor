#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: kernel ;
IN: arrayfactor.tests.sha2.input

TUPLE: input
    { node1# integer }
    { node2# integer }
    { node node }
    { input vector }
    port ; 

SYMBOL: Input

: input>vector ( string -- vector )
    over >vector reverse ;

: <input> ( node1# node2# string -- )
                    #! node1#
                    #! node2#
    over {node }    #! node1
    input>vector    #! input
    f               #! port
    input boa 
    dup Input set ;

ERROR: no-port-created-on-connect ;

: &&connect ( node#1 node#2 -- port/f )
    [ node#>nd ] bi@
    2dup connect dup
    [ pick maybe-create-ports 2nip ]
    [ 2nip no-port-created-on-connect ]
    if ;

: (on-step) ( seq -- )
    pop /b 
    #! Send !b instruction
;

: on-step ( -- )
    Input get input>>
    dup length 0 >
    [ (on-step) ] [ drop ] if ; 

 : start ( -- port )
     100 101
     [  "abc" <input> ]
     [ &&connect >>port ] bi ;