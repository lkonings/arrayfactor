#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: arrayfactor.chip.node arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation arrayfactor.ui.console
kernel ;
IN: arrayfactor.tests.sha2.receiver

&&chip

#! Reads 40 items from via b, and starts all over again.
101 {node}                  
this-compilation
" 0 org
  0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
  0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
  0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
  0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
  0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 ,
  : main 39 for @b !a+ unext
  : init      
    dup xor a!
    469 b! main
    [ 169 org ] init
" compile-source
this-compilation get-source
this-assembly assemble-source
this-assembly end-assembly


