#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.connection
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation
arrayfactor.chip.node.f18a.compilation.cfs
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.status
arrayfactor.chip.nodes
arrayfactor.chip.port
arrayfactor.chips
arrayfactor.core.library.macros.basic
arrayfactor.core.macros
arrayfactor.core.utils
arrayfactor.ui
arrayfactor.ui.chip.nodes
arrayfactor.ui.chip.nodes.navigation
arrays assocs combinators
fry grouping kernel locals logging math math.parser math.ranges
namespaces sequences words words.symbol ;
IN: arrayfactor.projects.sha2

!  CNT      CNT1    ROLL2   UTILS1   RESULT  H_       --N36-- MAJ    SIGMA0 ROLL0
!  BIGCNT   PADDING INIT1   INIT2    RESULT1 AH       AH_RQ   HCHMAJ CH     -BLINK-
!  OUTPUT   IO      IO_RQ   IO_RQ1   W_RQ1   MAINLOOP ML_AH   ML_AH1 SIGMA1 ROLL1
!  W4       W3      W2      W1       W_RQ    K_RQ     K1      K2     K3     STACK  

: NODES ( -- seq )
{
  "w4"       "w3"      "w2"          "w1"            "w_request"   "k_request"  "k1"          "k2"     "k3"     "stack"  
  "output"   "io"      "io_request"  "io_request1"   "w_request1"  "mainloop"   "ml_ah"       "ml_ah1" "s1"     "roll1"
  "bigcnt"   "padding" "init1"       "init2"         "result1"     "ah"         "ah_request"  "hchmaj" "ch"     "-n29-"
  "cnt"      "cnt1"    "roll2"       "utils1"        "result"      "h_"         "-n36-"       "maj"    "s0"     "roll0"
}
<enum> seq>> ;

: text>nd ( text -- nd/f ) NODES index ;

: +point ( w x y z -- w+y x+z ) rot* + [ + ] dip ;

ERROR: text>node#-error ;

: text>node# ( text -- node# )
    text>nd dup [ 10 /mod 0 0 +point swap 100 * + ] [ text>node#-error ] if ;

: define-nodename-macros ( -- )
    NODES [ [ text>node# number>string "[ " prepend " ]" append ] keep add-chip-macro ] each ;

! -----------------------------------------------------

: ld ( relative-path node# -- )
  {node}
  this-compilation get-cfs swap read-source
  this-compilation get-cfs translate-source ;

: set-connected-nodes ( connection seq -- ) { [ first prvnd ] [ second prvnd1 ] [ third nxtnd ] [ fourth nxtnd1 ] } 2cleave ;

! ERROR: start-at-word-error ;

:: compile+assemble-one ( cfs-file node-name node# start-at-word seq -- )
    cfs-file node# ld
    this-node node-name set-node-name
    this-compilation connection>> seq set-connected-nodes
    this-compilation dup get-cfs get-cfs-new-source " " join compile-source
    this-compilation get-source this-assembly assemble-source
    this-assembly end-assembly ;
!   start-at-word this-ram name>addr [ start-at-word-error ] unless this-assembly replace-a9-code ;

SYMBOL: Compile-Node-Named

: compile+assemble-node ( nodename -- )
    [ "projects/sha2/cfs/" swap append ".vf.cfs" append dup Compile-Node-Named set ]
    [ dup text>node# ] bi
    "init" { -1 -1 1 -1 } compile+assemble-one ;

: sha2 ( -- )
    <ga144> <chip> drop
    define-macros
    define-nodename-macros
    {
      "w1"
      "w2"
      "w3"
      "w4"
      "w_request1"
      "k1"
      "k2"
      "k3"
      "k_request"
      "io"
      "io_request"
      "io_request1"
      "init1"
      "init2"
      "cnt"
      "cnt1"
      "bigcnt"
      "result"
      "result1"
      "roll0"
      "roll1"
      "roll2"
      "ah"
      "ah_request"
      "hchmaj"
      "h_"
      "maj"
      "ch"
      "s0"
      "s1"
      "ml_ah"
      "ml_ah1"
      "output"
      "padding"
      "mainloop"
      "stack"
      "utils1"
      "w_request"
    } [ compile+assemble-node ] each
    init-ui
    start-stepping
!    40-nodes-window
!    navigation-window
    ;

: show-blocking ( -- seq )
    this-chip nodes>> [ dup [ dup status>> blocking-> [ opcode-> first ] [ drop f ] if ] when ] map ;

! USE: arrayfactor.projects.sha2

! tst "abc"
! BA 78 16 BF 8F 1 CF EA 41 41 40 DE 5D AE 22 23 B0 3 61 A3 96 17 7A 9C B4 10 FF 61 F2 0 15 AD
